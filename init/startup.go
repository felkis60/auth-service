package startup

import (
	"log"
	"path"
	"runtime"

	smsClient "gitlab.com/felkis60/auth-service/clients/sms-service"
	db "gitlab.com/felkis60/auth-service/database"
	l "gitlab.com/felkis60/auth-service/log"
	repos "gitlab.com/felkis60/auth-service/repository"

	"github.com/joho/godotenv"
)

func SystemStartup(logs_ bool, notTestEnv_ bool) {

	if !envInited {
		envInited = true
		if notTestEnv_ {
			if err := godotenv.Load("configs/.env"); err != nil {
				log.Fatal("ERROR: " + err.Error())
			}
		} else {
			_, filename, _, _ := runtime.Caller(0)
			dir := path.Join(path.Dir(filename), "..")
			log.Print(dir)
			if err := godotenv.Load(dir + "/configs/t.env"); err != nil {
				log.Fatal("ERROR: " + err.Error())
			}
		}
		repos.SetBruteforceVariablesFromEnv()
	}

	if logs_ {
		if !logsInited {
			logsInited = true
			l.StartLogs()
		}
	}

	db.InitRedis()

	if notTestEnv_ {
		smsClient.Init()
	}

	if !basicInited {
		basicInited = true
		db.InitMainPostgres()
		db.Migrate()
	}

}

var basicInited = false
var envInited = false
var logsInited = false
