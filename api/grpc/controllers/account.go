package controllers

import (
	"context"
	"encoding/json"

	pb "gitlab.com/felkis60/auth-service/proto"
	"gitlab.com/felkis60/auth-service/repository"
	types "gitlab.com/felkis60/auth-service/types"
)

type GRPCAccountServer struct {
	Repository repository.AccountRepository
	pb.UnimplementedAccountServiceServer
}

func (s *GRPCAccountServer) CreateAccount(ctx context.Context, req *pb.InputCreateByJSON) (*pb.ServiceResponse, error) {

	var tempInput types.InputCreateEditAccount
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	var account types.Account
	err := s.Repository.Create(&account, &tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonData, err := json.Marshal(account)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}
