package controllers

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"log"
	"strings"

	db "gitlab.com/felkis60/auth-service/database"
	logs "gitlab.com/felkis60/auth-service/log"
	pb "gitlab.com/felkis60/auth-service/proto"
	"gitlab.com/felkis60/auth-service/repository"
	repos "gitlab.com/felkis60/auth-service/repository"
	types "gitlab.com/felkis60/auth-service/types"
)

type GRPCUserServer struct {
	Repository repository.UserRepository
	pb.UnimplementedUserServiceServer
}

func (s *GRPCUserServer) Create(ctx context.Context, req *pb.InputCreateByJSON) (*pb.ServiceResponse, error) {

	var account types.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var tempInput types.InputCreateEditUser
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	data, err := s.Repository.Create(&tempInput, req.AccountToken, true)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCUserServer) Edit(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	var account types.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	var tempInput types.InputCreateEditUser
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}
	tempInput.JWTToken = &req.AuthToken

	data, err := s.Repository.Edit(req.UID, &tempInput, req.AccountToken)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCUserServer) Get(ctx context.Context, req *pb.InputGetByUID) (*pb.ServiceResponse, error) {

	var account types.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	data, err := s.Repository.Get(&types.GetOneUser{AccountToken: req.AccountToken, By: "uid", Value: req.UID})
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCUserServer) GetByID(ctx context.Context, req *pb.InputGetByID) (*pb.ServiceResponse, error) {

	var account types.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	data, err := s.Repository.Get(&types.GetOneUser{AccountToken: req.AccountToken, By: "id", Value: req.ID})
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCUserServer) GetByPhone(ctx context.Context, req *pb.InputGetByUID) (*pb.ServiceResponse, error) {

	var account types.Account
	if result := s.Repository.Db.Find(&account, "token = ?", req.AccountToken); result.RowsAffected == 0 {
		return &pb.ServiceResponse{Code: 400, Message: logs.ERRNoSuchToken}, errors.New(logs.ERRNoSuchToken)
	}

	data, err := s.Repository.Get(&types.GetOneUser{AccountToken: req.AccountToken, By: "phone", Value: req.UID})
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCUserServer) SendPhoneConfirmSMSCode(ctx context.Context, in *pb.InputLogin) (*pb.ServiceResponse, error) {

	log.Print(11)

	err := s.Repository.SendPhoneConfirmSMSCode(in.EmailORPhone, in.AccountToken, "")
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: nil}, nil

}

func (s *GRPCUserServer) LoginSMSPhone(ctx context.Context, in *pb.InputLogin) (*pb.ServiceResponse, error) {

	token, err := s.Repository.LoginSMSPhone(in.EmailORPhone, in.Password, in.AccountToken, "")
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonToken, err := json.Marshal(token)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonToken}, nil

}

func (s *GRPCUserServer) EmailLogin(ctx context.Context, in *pb.InputLogin) (*pb.ServiceResponse, error) {

	token, err := s.Repository.LoginEmail(in.EmailORPhone, in.Password, in.AccountToken, "")
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonToken, err := json.Marshal(token)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonToken}, nil

}

func (s *GRPCUserServer) PhoneLogin(ctx context.Context, in *pb.InputLogin) (*pb.ServiceResponse, error) {

	token, err := s.Repository.LoginPhone(in.EmailORPhone, in.Password, in.AccountToken, "")
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonToken, err := json.Marshal(token)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonToken}, nil

}

func (s *GRPCUserServer) CheckToken(ctx context.Context, in *pb.InputToken) (*pb.ServiceResponse, error) {

	claims, err := s.Repository.CheckToken(in.AccountToken, in.AuthToken)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonClaims, err := json.Marshal(claims)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonClaims}, nil

}

func (s *GRPCUserServer) RefreshToken(ctx context.Context, in *pb.InputToken) (*pb.ServiceResponse, error) {

	token, err := s.Repository.RefreshToken(in.AccountToken, in.AuthToken)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonToken, err := json.Marshal(token)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonToken}, nil

}

func (s *GRPCUserServer) Verify(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	var accRep = repos.AccountRepository{Db: db.Db}
	var account types.Account
	if err := accRep.Get(&account, req.AccountToken); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	var tempInput map[string]interface{}
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	user, err := s.Repository.Verify(req.UID, tempInput["verify_code"].(string), req.AccountToken)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonUser, err := json.Marshal(user)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonUser}, nil

}

func (s *GRPCUserServer) RequestChangePassword(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	var accRep = repos.AccountRepository{Db: db.Db}
	var account types.Account
	if err := accRep.Get(&account, req.AccountToken); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	var tempInput types.InputRequestChangePassword
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	err := s.Repository.SendChangePassLink(account.Token, &tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: nil}, nil

}

func (s *GRPCUserServer) ChangePassword(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	var accRep = repos.AccountRepository{Db: db.Db}
	var account types.Account
	if err := accRep.Get(&account, req.AccountToken); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	var tempInput types.InputChangeUserPass
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	err := s.Repository.ChangePassword(&tempInput, account.ID, "")
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: nil}, nil

}

func (s *GRPCUserServer) GetAllByID(ctx context.Context, req *pb.InputCreateByJSON) (*pb.ServiceResponse, error) {

	var accRep = repos.AccountRepository{Db: db.Db}
	var account types.Account
	if err := accRep.Get(&account, req.AccountToken); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	var tempInput types.InputGetAllUsersByID
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	data, err := s.Repository.GetAllByID(req.AccountToken, &tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCUserServer) List(ctx context.Context, req *pb.InputCreateByJSON) (*pb.ServiceResponse, error) {

	var accRep = repos.AccountRepository{Db: db.Db}
	var account types.Account
	if err := accRep.Get(&account, req.AccountToken); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	var tempInput types.InputGetListUser
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	data, err := s.Repository.List(req.AccountToken, &tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCUserServer) AccountChoice(ctx context.Context, in *pb.InputLogin) (*pb.ServiceResponse, error) {

	data, err := s.Repository.AccountChoice(in.EmailORPhone, in.Password, "")
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCUserServer) DoWithSecretToken(ctx context.Context, req *pb.InputGetByUID) (*pb.ServiceResponse, error) {

	basicAuth, err := base64.StdEncoding.DecodeString(req.UID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	var strArr = strings.Split(string(basicAuth), ":")
	if len(strArr) != 2 {
		err = errors.New("ERROR: Wrong token format")
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	data, err := s.Repository.DoWithSecretToken(strArr[1], strArr[0], "")
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCUserServer) GetSecretForPosiblyNewUserByPhone(ctx context.Context, req *pb.InputGetByUID) (*pb.ServiceResponse, error) {

	data, err := s.Repository.GetSecretForPosiblyNewUserByPhone(req.AccountToken, req.UID, "")
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCUserServer) LoginWithPhoneAndSecret(ctx context.Context, req *pb.InputLogin) (*pb.ServiceResponse, error) {

	data, err := s.Repository.LoginWithPhoneAndSecret(req.AccountToken, req.EmailORPhone, req.Password, "")
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, err
	}

	return &pb.ServiceResponse{Message: "Success!", Payload: jsonData}, nil

}
