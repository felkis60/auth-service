package main

import (
	"log"
	"net"
	"os"

	"gitlab.com/felkis60/auth-service/api/grpc/controllers"
	db "gitlab.com/felkis60/auth-service/database"
	start "gitlab.com/felkis60/auth-service/init"
	l "gitlab.com/felkis60/auth-service/log"
	pb "gitlab.com/felkis60/auth-service/proto"
	"gitlab.com/felkis60/auth-service/repository"

	"google.golang.org/grpc"
)

func main() {
	start.SystemStartup(true, true)
	lis, err := net.Listen("tcp", ":"+os.Getenv("GRPCPORT"))
	if err != nil {
		log.Fatalf(l.ERRListen, err)
	}

	s := grpc.NewServer()
	pb.RegisterAccountServiceServer(s, &controllers.GRPCAccountServer{Repository: repository.AccountRepository{Db: db.Db}})
	pb.RegisterUserServiceServer(s, &controllers.GRPCUserServer{Repository: repository.UserRepository{Db: db.Db}})

	log.Printf(l.INFStartServer, "grcp")

	if err := s.Serve(lis); err != nil {
		log.Fatalf(l.ERRServe, err)
	}

}
