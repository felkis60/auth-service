package router

import (
	controller "gitlab.com/felkis60/auth-service/api/rest/controllers"
	"gitlab.com/felkis60/auth-service/api/rest/middleware"
	db "gitlab.com/felkis60/auth-service/database"
	repos "gitlab.com/felkis60/auth-service/repository"

	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()

	v1 := r.Group("/v1")
	{
		{
			accounts := v1.Group("/accounts") //.Use(middleware.BlockIfBanned())
			rep := repos.AccountRepository{Db: db.Db}
			ctrl := &controller.AccountController{Repository: rep}

			accounts.POST("", ctrl.Create)
			accounts.POST("/", ctrl.Create)
			accounts.POST("/:token", ctrl.Get)
			accounts.POST("/:token/edit", ctrl.Edit)
		}

		{
			users := v1.Group("/users").Use(middleware.TokenLogin()) //.Use(middleware.BlockIfBanned())

			rep := repos.UserRepository{Db: db.Db}
			ctrl := &controller.UserController{Repository: rep}

			users.POST("", ctrl.Create)
			users.POST("/", ctrl.Create)
			users.POST("/phone-login", ctrl.PhoneLogin)
			users.POST("/sms-login", ctrl.LoginSMSPhone)
			users.POST("/email-login", ctrl.EmailLogin)
			users.POST("/nickname-login", ctrl.NicknameLogin)
			users.POST("/check-token", ctrl.CheckToken)
			users.POST("/refresh-token", ctrl.RefreshToken)
			users.GET("/verify", ctrl.Verify)
			users.POST("/change-password", ctrl.ChangePassword)
			users.POST("/request-change-password", ctrl.GenerateAndSendChangePassCode)
			users.POST("/send-email", ctrl.SendEmail)
			users.POST("/list", ctrl.UserList)
			users.POST("/account-choice", ctrl.AccountChoice)
			users.POST("/send-sms-code", ctrl.SendPhoneConfirmSMSCode)

			{
				usersUID := users.Use(middleware.UIDHandler())

				usersUID.POST("/:uid", ctrl.GetByUID)
				usersUID.GET("/:uid/send-verify-link", ctrl.SendEmailVerification)
				usersUID.POST("/:uid/edit", ctrl.Edit)
				usersUID.POST("/:uid/delete", ctrl.Delete)

			}
		}
	}
	return r
}
