package main

import (
	"bytes"
	"encoding/base64"
	b64 "encoding/base64"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	routes "gitlab.com/felkis60/auth-service/api/rest/router"
	db "gitlab.com/felkis60/auth-service/database"
	start "gitlab.com/felkis60/auth-service/init"
	logs "gitlab.com/felkis60/auth-service/log"
	repos "gitlab.com/felkis60/auth-service/repository"
	types "gitlab.com/felkis60/auth-service/types"

	convey "github.com/smartystreets/goconvey/convey"

	"github.com/gin-gonic/gin"
)

var router *gin.Engine

const rightToken = "123"
const rightToken2 = "1234"

var inited = false

func makeRequest(method string, url string, accountToken string, body interface{}, additionalHeaders *map[string]string) (int, *types.RestRespone) {
	var resp types.RestRespone
	w := httptest.NewRecorder()

	var marshaledBody []byte
	var req *http.Request
	var err error
	if body != nil {
		marshaledBody, err = json.Marshal(body)
		if err != nil {
			return 0, nil
		}
		req, err = http.NewRequest(method, url, bytes.NewReader(marshaledBody))
		if err != nil {
			return 0, nil
		}
	} else {
		req, err = http.NewRequest(method, url, nil)
		if err != nil {
			return 0, nil
		}
	}
	req.Header.Set("Token", accountToken)
	if additionalHeaders != nil {
		for key, value := range *additionalHeaders {
			req.Header.Set(key, value)
		}
	}
	router.ServeHTTP(w, req)
	err = json.Unmarshal(w.Body.Bytes(), &resp)
	if err != nil {
		return 0, nil
	}

	return w.Code, &resp
}

func setup() {
	if !inited {

		start.SystemStartup(true, false)
		router = routes.SetupRouter()
		inited = true
	}

	var result int
	db.Db.Raw("DELETE FROM accounts").Scan(&result)
	db.Db.Raw("DELETE FROM users").Scan(&result)
	db.Db.Raw("DELETE FROM login_histories").Scan(&result)

	db.Db.Create(&types.Account{
		Name:                    "test",
		Token:                   rightToken,
		AccessTokenTime:         300,
		RefreshTokenTime:        600,
		OldPasswordCheck:        true,
		Domain:                  "https://adasefwefagfasfe.com",
		VerifyEmailEndpoint:     "/verify",
		ChangePasswordEndpoint:  "/change-password",
		VerifyMailTitle:         "Verify your account",
		ChangePasswordMailTitle: "Change password",
		VerifyMailBody:          "Your verification code is: ",
		ChangePasswordMailBody:  "Change password: ",
		DialerHost:              "smtp.yandex.ru",
		DialerPort:              465,
		DialerEmail:             "kryukov@fondo.ru",
		DialerPassword:          "60faabee",
		DialerFromMask:          "kryukov@fondo.ru"})

	db.Db.Create(&types.Account{
		Name:                    "test",
		Token:                   rightToken2,
		AccessTokenTime:         300,
		RefreshTokenTime:        600,
		Domain:                  "https://adasefwefagfasfe.com",
		VerifyEmailEndpoint:     "/verify",
		ChangePasswordEndpoint:  "/change-password",
		VerifyMailTitle:         "Verify your account",
		ChangePasswordMailTitle: "Change password",
		VerifyMailBody:          "Your verification code is: ",
		ChangePasswordMailBody:  "Change password: ",
		DialerHost:              "smtp.yandex.ru",
		DialerPort:              465,
		DialerEmail:             "kryukov@fondo.ru",
		DialerPassword:          "60faabee",
		DialerFromMask:          "kryukov@fondo.ru"})

}

func TestAll(t *testing.T) {

	convey.Convey("Auth Service Tests", t, func() {
		setup()

		convey.Convey("Create 3 Users and verify them", func() {

			var testEmail1 = "test1@gmail.com"
			var testEmail2 = "test2@gmail.com"
			var testEmail3 = "test3@gmail.com"
			var testName1 = "temp1"
			var testName2 = "temp2"
			var testName3 = "temp3"
			var testPass = "123"

			//User 1
			var code, _ = makeRequest("POST",
				"/v1/users/",
				rightToken,
				&types.InputCreateEditUser{Name: &testName1, LastName: &testName1, Email: &testEmail1, Password: &testPass},
				nil)
			convey.So(code, convey.ShouldEqual, 200)

			var createdUser1 types.User
			result := db.Db.Last(&createdUser1)
			convey.So(result.RowsAffected, convey.ShouldEqual, 1)
			convey.So(createdUser1.EmailVerify, convey.ShouldEqual, false)

			verificationCode, err := repos.GenerateVerificationCodeEmail(&createdUser1)
			convey.So(err, convey.ShouldBeNil)

			code, _ = makeRequest("GET",
				"/v1/users/verify?user_uid="+createdUser1.UID+"&code="+verificationCode,
				rightToken,
				nil,
				nil)
			convey.So(code, convey.ShouldEqual, 200)

			result = db.Db.Last(&createdUser1)
			convey.So(result.RowsAffected, convey.ShouldEqual, 1)
			convey.So(createdUser1.EmailVerify, convey.ShouldEqual, true)

			//User 2
			code, _ = makeRequest("POST",
				"/v1/users/",
				rightToken,
				&types.InputCreateEditUser{Name: &testName2, LastName: &testName2, Email: &testEmail2, Password: &testPass},
				nil)
			convey.So(code, convey.ShouldEqual, 200)

			var createdUser2 types.User
			result = db.Db.Last(&createdUser2)
			convey.So(result.RowsAffected, convey.ShouldEqual, 1)
			convey.So(createdUser2.EmailVerify, convey.ShouldEqual, false)

			verificationCode, err = repos.GenerateVerificationCodeEmail(&createdUser2)
			convey.So(err, convey.ShouldBeNil)

			code, _ = makeRequest("GET",
				"/v1/users/verify?user_uid="+createdUser2.UID+"&code="+verificationCode,
				rightToken,
				nil,
				nil)
			convey.So(code, convey.ShouldEqual, 200)

			result = db.Db.Last(&createdUser2)
			convey.So(result.RowsAffected, convey.ShouldEqual, 1)
			convey.So(createdUser2.EmailVerify, convey.ShouldEqual, true)

			//User 3
			code, _ = makeRequest("POST",
				"/v1/users/",
				rightToken,
				&types.InputCreateEditUser{Name: &testName3, LastName: &testName3, Email: &testEmail3, Password: &testPass},
				nil)
			convey.So(code, convey.ShouldEqual, 200)

			var createdUser3 types.User
			result = db.Db.Last(&createdUser3)
			convey.So(result.RowsAffected, convey.ShouldEqual, 1)
			convey.So(createdUser3.EmailVerify, convey.ShouldEqual, false)

			verificationCode, err = repos.GenerateVerificationCodeEmail(&createdUser3)
			convey.So(err, convey.ShouldBeNil)

			code, _ = makeRequest("GET",
				"/v1/users/verify?user_uid="+createdUser3.UID+"&code="+verificationCode,
				rightToken,
				nil,
				nil)
			convey.So(code, convey.ShouldEqual, 200)

			result = db.Db.Last(&createdUser3)
			convey.So(result.RowsAffected, convey.ShouldEqual, 1)
			convey.So(createdUser3.EmailVerify, convey.ShouldEqual, true)

			convey.Convey("Try create the same 3 Users, must fail", func() {
				//User 1
				var code, _ = makeRequest("POST",
					"/v1/users/",
					rightToken,
					&types.InputCreateEditUser{Name: &testName1, LastName: &testName1, Email: &testEmail1, Password: &testPass},
					nil)
				convey.So(code, convey.ShouldEqual, 400)

				//User 2
				code, _ = makeRequest("POST",
					"/v1/users/",
					rightToken,
					&types.InputCreateEditUser{Name: &testName2, LastName: &testName2, Email: &testEmail2, Password: &testPass},
					nil)
				convey.So(code, convey.ShouldEqual, 400)

				//User 3
				code, _ = makeRequest("POST",
					"/v1/users/",
					rightToken,
					&types.InputCreateEditUser{Name: &testName3, LastName: &testName3, Email: &testEmail3, Password: &testPass},
					nil)
				convey.So(code, convey.ShouldEqual, 400)

			})

			convey.Convey("Create the same 2 first Users in another account", func() {
				//User 1
				var code, _ = makeRequest("POST",
					"/v1/users/",
					rightToken2,
					&types.InputCreateEditUser{Name: &testName1, LastName: &testName1, Email: &testEmail1, Password: &testPass},
					nil)
				convey.So(code, convey.ShouldEqual, 200)

				var createdUser1 types.User
				result := db.Db.Last(&createdUser1)
				convey.So(result.RowsAffected, convey.ShouldEqual, 1)
				convey.So(createdUser1.EmailVerify, convey.ShouldEqual, false)

				verificationCode, err = repos.GenerateVerificationCodeEmail(&createdUser1)
				convey.So(err, convey.ShouldBeNil)

				code, _ = makeRequest("GET",
					"/v1/users/verify?user_uid="+createdUser1.UID+"&code="+verificationCode,
					rightToken2,
					nil,
					nil)
				convey.So(code, convey.ShouldEqual, 200)

				result = db.Db.Last(&createdUser1)
				convey.So(result.RowsAffected, convey.ShouldEqual, 1)
				convey.So(createdUser1.EmailVerify, convey.ShouldEqual, true)

				//User 2
				code, _ = makeRequest("POST",
					"/v1/users/",
					rightToken2,
					&types.InputCreateEditUser{Name: &testName2, LastName: &testName2, Email: &testEmail2, Password: &testPass},
					nil)
				convey.So(code, convey.ShouldEqual, 200)

				var createdUser2 types.User
				result = db.Db.Last(&createdUser2)
				convey.So(result.RowsAffected, convey.ShouldEqual, 1)
				convey.So(createdUser2.EmailVerify, convey.ShouldEqual, false)

				verificationCode, err = repos.GenerateVerificationCodeEmail(&createdUser2)
				convey.So(err, convey.ShouldBeNil)

				code, _ = makeRequest("GET",
					"/v1/users/verify?user_uid="+createdUser2.UID+"&code="+verificationCode,
					rightToken2,
					nil,
					nil)
				convey.So(code, convey.ShouldEqual, 200)

				result = db.Db.Last(&createdUser2)
				convey.So(result.RowsAffected, convey.ShouldEqual, 1)
				convey.So(createdUser2.EmailVerify, convey.ShouldEqual, true)

				convey.Convey("User1 and User2 must return two accounts, User3 must return one account in AccountChoice", func() {
					//User 1
					var code, resp = makeRequest("POST",
						"/v1/users/account-choice",
						rightToken2,
						"{\n}",
						&map[string]string{"Authorization": "Basic " + base64.StdEncoding.EncodeToString([]byte("test1@gmail.com:123"))})
					convey.So(code, convey.ShouldEqual, 200)
					convey.So(len(resp.Payload.([]interface{})), convey.ShouldEqual, 2)

					//User 2
					code, resp = makeRequest("POST",
						"/v1/users/account-choice",
						rightToken2,
						nil,
						&map[string]string{"Authorization": "Basic " + base64.StdEncoding.EncodeToString([]byte("test2@gmail.com:123"))})
					convey.So(code, convey.ShouldEqual, 200)
					convey.So(len(resp.Payload.([]interface{})), convey.ShouldEqual, 2)

					//User 3
					code, resp = makeRequest("POST",
						"/v1/users/account-choice",
						rightToken2,
						nil,
						&map[string]string{"Authorization": "Basic " + base64.StdEncoding.EncodeToString([]byte("test3@gmail.com:123"))})
					convey.So(code, convey.ShouldEqual, 200)
					convey.So(len(resp.Payload.([]interface{})), convey.ShouldEqual, 1)

				})

			})
		})

		convey.Convey("User Verify and Login", func() {

			convey.Convey("Create User 1", func() {
				var tempUserName = "tempasehuwegu453tgw34tv254ybuawe9b"
				var tempUserLastName = "tem"
				var tempUserEmail = "  te st @gm ai l.com  "
				var tempUserPassword = "123"
				var tempUserNickname = "nickname"
				var tempExtID = int64(10)
				var code, resp = makeRequest("POST",
					"/v1/users/",
					rightToken,
					&types.InputCreateEditUser{Nickname: &tempUserNickname, Name: &tempUserName, LastName: &tempUserLastName, Email: &tempUserEmail, Password: &tempUserPassword, ExtID: &tempExtID},
					nil)
				convey.So(code, convey.ShouldEqual, 200)

				convey.Convey("User 1 must be in test db, not verified", func() {

					var createdUser types.User
					result := db.Db.First(&createdUser, "name = ?", tempUserName)
					convey.So(result.RowsAffected, convey.ShouldEqual, 1)
					convey.So(createdUser.EmailVerify, convey.ShouldEqual, false)

					convey.Convey("Verify User 1", func() {

						verificationCode, err := repos.GenerateVerificationCodeEmail(&createdUser)
						convey.So(err, convey.ShouldBeNil)

						code, resp = makeRequest("GET",
							"/v1/users/verify?user_uid="+createdUser.UID+"&code="+verificationCode,
							rightToken,
							nil,
							nil)
						convey.So(code, convey.ShouldEqual, 200)

						convey.Convey("User 1 must be in test db, verified", func() {

							result = db.Db.First(&createdUser, "name = ?", tempUserName)
							convey.So(result.RowsAffected, convey.ShouldEqual, 1)
							convey.So(createdUser.EmailVerify, convey.ShouldEqual, true)

							convey.Convey("Email Login for User 1", func() {

								code, resp = makeRequest("POST",
									"/v1/users/email-login",
									rightToken,
									nil,
									&map[string]string{"Authorization": "Basic " + b64.StdEncoding.EncodeToString([]byte(*createdUser.Email+":"+tempUserPassword))})
								convey.So(code, convey.ShouldEqual, 200)

								var tempAccessToken = resp.Payload.(map[string]interface{})["access_token"]
								var tempRefreshToken = resp.Payload.(map[string]interface{})["refresh_token"]
								log.Print(tempAccessToken, tempRefreshToken)

								convey.Convey("Check Access Token from User 1", func() {

									code, resp = makeRequest("POST",
										"/v1/users/check-token",
										rightToken,
										nil,
										&map[string]string{"Authorization": "Bearer " + tempAccessToken.(string)})

									log.Print(resp)
									convey.So(code, convey.ShouldEqual, 200)

									convey.Convey("Refresh Token for User 1", func() {

										code, resp = makeRequest("POST",
											"/v1/users/refresh-token",
											rightToken,
											nil,
											&map[string]string{"Authorization": "Bearer " + tempRefreshToken.(string)})

										convey.So(code, convey.ShouldEqual, 200)

									})
								})
							})
						})
					})
				})
			})
		})

		convey.Convey("Create Users with different Accounts", func() {
			convey.Convey("Create User 1, Account 1", func() {
				var tempUserName = "temp"
				var tempUserName2 = "temp2"
				//var tempUserPhone = "123"
				//var tempUserPhone2 = "1234"
				var tempUserLastName = "tem"
				var tempUserEmail = "test1@gmail.com"
				var tempUserEmail2 = "test2@gmail.com"
				var tempUserPassword = "123"
				var tempUserNickname = "nickname1"
				//var tempUserNickname2 = "nickname2"
				var code, resp = makeRequest("POST",
					"/v1/users/",
					rightToken,
					&types.InputCreateEditUser{Nickname: &tempUserNickname, Name: &tempUserName, LastName: &tempUserLastName, Email: &tempUserEmail, Password: &tempUserPassword},
					nil)
				convey.So(code, convey.ShouldEqual, 200)

				convey.Convey("Verify User 1", func() {

					var createdUser types.User
					db.Db.First(&createdUser, "email = ?", tempUserEmail)

					verificationCode, err := repos.GenerateVerificationCodeEmail(&createdUser)
					convey.So(err, convey.ShouldBeNil)

					code, resp = makeRequest("GET",
						"/v1/users/verify?user_uid="+createdUser.UID+"&code="+verificationCode,
						rightToken,
						nil,
						nil)
					convey.So(code, convey.ShouldEqual, 200)

					convey.Convey("Create User 2, same as User 1, Account 2", func() {

						code, resp = makeRequest("POST",
							"/v1/users/",
							rightToken2,
							&types.InputCreateEditUser{Nickname: &tempUserNickname, Name: &tempUserName2, LastName: &tempUserLastName, Email: &tempUserEmail, Password: &tempUserPassword},
							nil)
						convey.So(code, convey.ShouldEqual, 200)

						convey.Convey("Verify User 2", func() {

							var createdUser2 types.User
							db.Db.First(&createdUser2, "name = ?", tempUserName2)

							verificationCode, err = repos.GenerateVerificationCodeEmail(&createdUser2)
							convey.So(err, convey.ShouldBeNil)

							code, resp = makeRequest("GET",
								"/v1/users/verify?user_uid="+createdUser2.UID+"&code="+verificationCode,
								rightToken2,
								nil,
								nil)
							convey.So(code, convey.ShouldEqual, 200)

							convey.Convey("Try create User 3, same as User 2, Account 2, must fail", func() {

								code, resp = makeRequest("POST",
									"/v1/users/",
									rightToken2,
									&types.InputCreateEditUser{Name: &tempUserName, LastName: &tempUserLastName, Email: &tempUserEmail, Password: &tempUserPassword},
									nil)
								convey.So(code, convey.ShouldEqual, 400)
								convey.So(resp.Message, convey.ShouldEqual, logs.ERRUsersAlreadyExistsEmail)

								convey.Convey("Try create User 3, with same Nickname as User 2, Account 2, must fail", func() {

									code, resp = makeRequest("POST",
										"/v1/users/",
										rightToken2,
										&types.InputCreateEditUser{Nickname: &tempUserNickname, Name: &tempUserName, LastName: &tempUserLastName, Email: &tempUserEmail2, Password: &tempUserPassword},
										nil)
									convey.So(code, convey.ShouldEqual, 400)
									convey.So(resp.Message, convey.ShouldEqual, logs.ERRUsersAlreadyExistsNickname)

									//convey.Convey("Try create User 3, with same Phone as User 2, Account 2, must fail", func() {
									//
									//	code, resp = makeRequest("POST",
									//		"/v1/users/",
									//		rightToken2,
									//		&types.InputCreateEditUser{Nickname: &tempUserNickname2, Name: &tempUserName, LastName: &tempUserLastName, Email: &tempUserEmail2, Password: &tempUserPassword},
									//		nil)
									//	convey.So(code, convey.ShouldEqual, 400)
									//	convey.So(resp.Message, convey.ShouldEqual, logs.ERRUsersAlreadyExistsPhone)
									//
									//})
								})
							})
						})

					})
				})
			})

		})

		convey.Convey("Change User`s Password", func() {
			convey.Convey("Create User 1, Account 1", func() {
				var tempUserName = "temp"
				//var tempUserPhone = "123"
				var tempUserLastName = "tem"
				var tempUserEmail = "test1@gmail.com"
				var tempUserPassword = "123"
				var tempUserNickname = "nickname1"
				var code, resp = makeRequest("POST",
					"/v1/users/",
					rightToken,
					&types.InputCreateEditUser{Nickname: &tempUserNickname, Name: &tempUserName, LastName: &tempUserLastName, Email: &tempUserEmail, Password: &tempUserPassword},
					nil)
				convey.So(code, convey.ShouldEqual, 200)

				var user types.User
				db.Db.First(&user, "email = ?", tempUserEmail)

				convey.Convey("Try get Change Password Code, must fail", func() {
					code, resp = makeRequest("POST",
						"/v1/users/request-change-password",
						rightToken,
						&types.InputRequestChangePassword{Email: tempUserEmail},
						nil)
					convey.So(code, convey.ShouldEqual, 400)
					convey.So(resp.Message, convey.ShouldEqual, logs.ERRNoSuchUser)

					convey.Convey("Verify User 1", func() {

						var createdUser types.User
						db.Db.First(&createdUser, "email = ?", tempUserEmail)

						verificationCode, err := repos.GenerateVerificationCodeEmail(&createdUser)
						convey.So(err, convey.ShouldBeNil)

						code, resp = makeRequest("GET",
							"/v1/users/verify?user_uid="+createdUser.UID+"&code="+verificationCode,
							rightToken,
							nil,
							nil)
						convey.So(code, convey.ShouldEqual, 200)

						convey.Convey("Get Change Password Code", func() {
							code, resp = makeRequest("POST",
								"/v1/users/request-change-password",
								rightToken,
								&types.InputRequestChangePassword{Email: tempUserEmail},
								nil)
							convey.So(code, convey.ShouldEqual, 200)

							verificationCode, err = repos.GenerateChangePassCode(&user)
							convey.So(err, convey.ShouldBeNil)

							convey.Convey("Try Change User's Password with wrong Code, must fail", func() {
								code, resp = makeRequest("POST",
									"/v1/users/change-password",
									rightToken,
									&types.InputChangeUserPass{NewPassword: "1234", OldPassword: &tempUserPassword, UserUID: user.UID, Code: verificationCode + "213432"},
									nil)
								convey.So(code, convey.ShouldEqual, 400)

								convey.Convey("Try Change User's Password, without old password, must fail", func() {
									code, resp = makeRequest("POST",
										"/v1/users/change-password",
										rightToken,
										&types.InputChangeUserPass{NewPassword: "1234", UserUID: user.UID, Code: verificationCode},
										nil)
									convey.So(code, convey.ShouldEqual, 400)

									convey.Convey("Change User's Password", func() {
										code, resp = makeRequest("POST",
											"/v1/users/change-password",
											rightToken,
											&types.InputChangeUserPass{NewPassword: "1234", OldPassword: &tempUserPassword, UserUID: user.UID, Code: verificationCode},
											nil)
										convey.So(code, convey.ShouldEqual, 200)

										convey.Convey("Try Change User's Password with old Code, must fail", func() {
											code, resp = makeRequest("POST",
												"/v1/users/change-password",
												rightToken,
												&types.InputChangeUserPass{NewPassword: "1234", OldPassword: &tempUserPassword, UserUID: user.UID, Code: verificationCode},
												nil)
											convey.So(code, convey.ShouldEqual, 400)

										})
									})
								})
							})
						})
					})
				})
			})
		})

		convey.Convey("Try change User`s email", func() {
			var tempUserName = "temp"
			var tempUserLastName = "tem"
			var tempUserEmail = "test1@gmail.com"
			var tempUserEmail2 = "test2@gmail.com"
			var tempUserEmail3 = "test3@gmail.com"
			var tempNewUserEmail3 = "test33@gmail.com"
			//var tempUserPhone = "123"
			//var tempUserPhone2 = "1234"
			var tempUserPassword = "password"
			var tempUserNickname = "nickname1"
			var tempUserNickname2 = "nickname2"
			var tempUserNickname3 = "nickname3"

			convey.Convey("Create User 1", func() {

				var code, resp = makeRequest("POST",
					"/v1/users/",
					rightToken,
					&types.InputCreateEditUser{Nickname: &tempUserNickname, Name: &tempUserName, LastName: &tempUserLastName, Email: &tempUserEmail, Password: &tempUserPassword},
					nil)
				convey.So(code, convey.ShouldEqual, 200)

				convey.Convey("Verify User 1", func() {

					var createdUser types.User
					db.Db.First(&createdUser, "email = ?", tempUserEmail)

					verificationCode, err := repos.GenerateVerificationCodeEmail(&createdUser)
					convey.So(err, convey.ShouldBeNil)

					code, resp = makeRequest("GET",
						"/v1/users/verify?user_uid="+createdUser.UID+"&code="+verificationCode,
						rightToken,
						nil,
						nil)
					convey.So(code, convey.ShouldEqual, 200)

					convey.Convey("Create User 2", func() {

						code, resp = makeRequest("POST",
							"/v1/users/",
							rightToken,
							&types.InputCreateEditUser{Nickname: &tempUserNickname2, Name: &tempUserName, LastName: &tempUserLastName, Email: &tempUserEmail2, Password: &tempUserPassword},
							nil)
						convey.So(code, convey.ShouldEqual, 200)

						convey.Convey("Verify User 2", func() {

							var createdUser2 types.User
							db.Db.First(&createdUser2, "email = ?", tempUserEmail2)

							verificationCode, err = repos.GenerateVerificationCodeEmail(&createdUser2)
							convey.So(err, convey.ShouldBeNil)

							code, resp = makeRequest("GET",
								"/v1/users/verify?user_uid="+createdUser2.UID+"&code="+verificationCode,
								rightToken,
								nil,
								nil)
							convey.So(code, convey.ShouldEqual, 200)

							convey.Convey("Create User 3", func() {

								code, resp = makeRequest("POST",
									"/v1/users/",
									rightToken,
									&types.InputCreateEditUser{Nickname: &tempUserNickname3, Name: &tempUserName, LastName: &tempUserLastName, Email: &tempUserEmail3, Password: &tempUserPassword},
									nil)
								convey.So(code, convey.ShouldEqual, 200)

								convey.Convey("Verify User 3", func() {

									var createdUser3 types.User
									db.Db.First(&createdUser3, "email = ?", tempUserEmail3)

									verificationCode, err = repos.GenerateVerificationCodeEmail(&createdUser3)
									convey.So(err, convey.ShouldBeNil)

									code, resp = makeRequest("GET",
										"/v1/users/verify?user_uid="+createdUser3.UID+"&code="+verificationCode,
										rightToken,
										nil,
										nil)
									convey.So(code, convey.ShouldEqual, 200)

									convey.Convey("Email Login for User 3", func() {

										code, resp = makeRequest("POST",
											"/v1/users/email-login",
											rightToken,
											nil,
											&map[string]string{"Authorization": "Basic " + b64.StdEncoding.EncodeToString([]byte(*createdUser3.Email+":"+tempUserPassword))})
										convey.So(code, convey.ShouldEqual, 200)

										var tempAccessToken = resp.Payload.(map[string]interface{})["access_token"]
										var tempRefreshToken = resp.Payload.(map[string]interface{})["refresh_token"]
										log.Print(tempAccessToken, tempRefreshToken)

										convey.Convey("Check Access Token from User 3", func() {

											code, resp = makeRequest("POST",
												"/v1/users/check-token",
												rightToken,
												nil,
												&map[string]string{"Authorization": "Bearer " + tempAccessToken.(string)})

											log.Print(resp)
											convey.So(code, convey.ShouldEqual, 200)

											convey.Convey("Try Change User's 3 Email into User's 1 Email, must fail", func() {

												code, resp = makeRequest("POST",
													"/v1/users/"+createdUser3.UID+"/edit",
													rightToken,
													&types.InputCreateEditUser{Name: &tempUserName, LastName: &tempUserLastName, Email: &tempUserEmail},
													&map[string]string{"Authorization": "Bearer " + tempAccessToken.(string)})

												convey.So(code, convey.ShouldEqual, 400)
												convey.So(resp.Message, convey.ShouldEqual, logs.ERRUsersAlreadyExistsEmail)

												convey.Convey("Change User's 3 Email", func() {

													code, resp = makeRequest("POST",
														"/v1/users/"+createdUser3.UID+"/edit",
														rightToken,
														&types.InputCreateEditUser{Name: &tempUserName, LastName: &tempUserLastName, Email: &tempNewUserEmail3},
														&map[string]string{"Authorization": "Bearer " + tempAccessToken.(string)})

													db.Db.First(&createdUser3)
													convey.So(code, convey.ShouldEqual, 200)
													convey.So(createdUser3.EmailVerify, convey.ShouldEqual, true)
													convey.So(createdUser3.Email, convey.ShouldNotBeNil)
													convey.So(*createdUser3.Email, convey.ShouldEqual, tempUserEmail3)
													convey.So(createdUser3.NewEmail, convey.ShouldNotBeNil)
													convey.So(*createdUser3.NewEmail, convey.ShouldEqual, tempNewUserEmail3)

													convey.Convey("Confirm New User's 3 Email", func() {

														verificationCode, err = repos.GenerateVerificationCodeEmail(&createdUser3)
														convey.So(err, convey.ShouldBeNil)

														code, resp = makeRequest("GET",
															"/v1/users/verify?user_uid="+createdUser3.UID+"&code="+verificationCode,
															rightToken,
															nil,
															nil)

														db.Db.First(&createdUser3)
														convey.So(code, convey.ShouldEqual, 200)
														convey.So(createdUser3.EmailVerify, convey.ShouldEqual, true)
														convey.So(createdUser3.Email, convey.ShouldNotBeNil)
														convey.So(*createdUser3.Email, convey.ShouldEqual, tempNewUserEmail3)
														convey.So(createdUser3.NewEmail, convey.ShouldBeNil)

													})
												})
											})
										})
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
