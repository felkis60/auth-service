package controllers

import (
	"encoding/base64"
	"strings"

	"gitlab.com/felkis60/auth-service/repository"
	types "gitlab.com/felkis60/auth-service/types"

	"github.com/gin-gonic/gin"
)

type UserController struct {
	Repository repository.UserRepository
}

// @Summary Create new user
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_     body types.InputCreateEditUser true "Request body"
// @Success 200 {object} types.User
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/ [post]
func (ctrl *UserController) Create(c *gin.Context) {

	var input types.InputCreateEditUser
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	user, err := ctrl.Repository.Create(&input, c.MustGet("token").(string), true)
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: user})
}

// @Summary Delete user
// @Tags Users
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param 	_ 	body types.InputDeleteUser true "Request body"
// @Param uid path string true "User UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/{uid}/delete [post]
func (ctrl *UserController) Delete(c *gin.Context) {
	var input types.InputDeleteUser
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	input.AccountToken = c.MustGet("token").(string)
	input.UID = c.MustGet("uid").(string)

	data, err := ctrl.Repository.Delete(&input)
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Edit user
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	Authorization header string true "Access Token"
// @Param 	_ 	body types.InputCreateEditUser true "Request body"
// @Param uid path string true "User UID"
// @Success 200 {object} types.User
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/{uid}/edit [post]
func (ctrl *UserController) Edit(c *gin.Context) {

	// var inputHeader types.InputGinHeader
	// if err := c.ShouldBindHeader(&inputHeader); err != nil {
	// 	c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
	// 	return
	// }

	// strArr := strings.Split(inputHeader.Authorization, " ")
	// if len(strArr) != 2 {
	// 	c.JSON(400, types.RestRespone{Message: "ERROR: Wrong header!", Payload: nil})
	// 	return
	// }

	var input types.InputCreateEditUser
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	// input.JWTToken = &strArr[1]

	user, err := ctrl.Repository.Edit(c.MustGet("uid").(string), &input, c.MustGet("token").(string))
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: user})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: user})

}

// @Summary Get one user
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "User UID"
// @Success 200 {object} types.User
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/{uid} [post]
func (ctrl *UserController) GetByUID(c *gin.Context) {

	user, err := ctrl.Repository.Get(&types.GetOneUser{AccountToken: c.MustGet("token").(string), By: "uid", Value: c.MustGet("uid").(string), SetSecretToken: true})
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: user})
}

// @Summary Check access token
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param   Token header string true "Account Token"
// @Param 	Authorization header string true "Access Token"
// @Success 200 {object} string "map[string]interface{} fields:'user_id', 'user_ext_id', 'user_nickname', 'user_name', 'user_phone', 'user_email', 'exp'"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/check-token [post]
func (ctrl *UserController) CheckToken(c *gin.Context) {

	var input types.InputGinHeader
	if err := c.ShouldBindHeader(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	strArr := strings.Split(input.Authorization, " ")
	if len(strArr) != 2 {
		c.JSON(400, types.RestRespone{Message: "ERROR: Wrong header!", Payload: nil})
		return
	}

	tokenData, err := ctrl.Repository.CheckToken(c.MustGet("token").(string), strArr[1])
	if err != nil {
		if err.Error() == "Token is expired" {
			c.JSON(401, types.RestRespone{Message: err.Error(), Payload: nil})
			return

		}
		c.JSON(403, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: tokenData})
}

// @Summary Try to refresh tokens
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	Authorization header string true "Refresh Token"
// @Success 200 {object} types.JWTToken
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/refresh-token [post]
func (ctrl *UserController) RefreshToken(c *gin.Context) {

	var input types.InputGinHeader
	if err := c.ShouldBindHeader(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	strArr := strings.Split(input.Authorization, " ")
	if len(strArr) != 2 {
		c.JSON(400, types.RestRespone{Message: "ERROR: Wrong header!", Payload: nil})
		return
	}

	tokenData, err := ctrl.Repository.RefreshToken(c.MustGet("token").(string), strArr[1])
	if err != nil {
		if err.Error() == "Token is expired" {
			c.JSON(401, types.RestRespone{Message: err.Error(), Payload: nil})
			return

		}
		c.JSON(403, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: tokenData})
}

// @Summary Login with email
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	Authorization header string true "Email:Password"
// @Success 200 {object} types.JWTToken
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/email-login [post]
func (ctrl *UserController) EmailLogin(c *gin.Context) {

	var input types.InputGinHeader
	if err := c.ShouldBindHeader(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	strArr := strings.Split(string(input.Authorization), " ")
	if len(strArr) != 2 {
		c.JSON(400, types.RestRespone{Message: "ERROR: Wrong login header!", Payload: nil})
		return
	}

	basicAuth, err := base64.StdEncoding.DecodeString(strArr[1])
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	strArr = strings.Split(string(basicAuth), ":")
	if len(strArr) != 2 {
		c.JSON(400, types.RestRespone{Message: "ERROR: Wrong login header!", Payload: nil})
		return
	}

	tokens, err := ctrl.Repository.LoginEmail(strArr[0], strArr[1], c.MustGet("token").(string), c.ClientIP())
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: tokens})
}

// @Summary Login with nickname
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	Authorization header string true "Nickname:Password"
// @Success 200 {object} types.JWTToken
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/nickname-login [post]
func (ctrl *UserController) NicknameLogin(c *gin.Context) {

	var input types.InputGinHeader
	if err := c.ShouldBindHeader(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	strArr := strings.Split(string(input.Authorization), " ")
	if len(strArr) != 2 {
		c.JSON(400, types.RestRespone{Message: "ERROR: Wrong login header!", Payload: nil})
		return
	}

	basicAuth, err := base64.StdEncoding.DecodeString(strArr[1])
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	strArr = strings.Split(string(basicAuth), ":")
	if len(strArr) != 2 {
		c.JSON(400, types.RestRespone{Message: "ERROR: Wrong login header!", Payload: nil})
		return
	}

	tokens, err := ctrl.Repository.LoginNickname(strArr[0], strArr[1], c.MustGet("token").(string), c.ClientIP())
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: tokens})
}

// @Summary Login with phone
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	Authorization header string true "Phone:Password"
// @Success 200 {object} types.JWTToken
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/phone-login [post]
func (ctrl *UserController) PhoneLogin(c *gin.Context) {

	var input types.InputGinHeader
	if err := c.ShouldBindHeader(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	strArr := strings.Split(string(input.Authorization), " ")
	if len(strArr) != 2 {
		c.JSON(400, types.RestRespone{Message: "ERROR: Wrong login header!", Payload: nil})
		return
	}

	basicAuth, err := base64.StdEncoding.DecodeString(strArr[1])
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	strArr = strings.Split(string(basicAuth), ":")
	if len(strArr) != 2 {
		c.JSON(400, types.RestRespone{Message: "ERROR: Wrong login header!", Payload: nil})
		return
	}

	tokens, err := ctrl.Repository.LoginPhone(strArr[0], strArr[1], c.MustGet("token").(string), c.ClientIP())
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: tokens})
}

// @Summary Verify User
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param user_uid query string true "User UID"
// @Param code query string true "Verification Code"
// @Success 200 {object} types.User
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/verify [get]
func (ctrl *UserController) Verify(c *gin.Context) {

	uid, ok := c.GetQuery("user_uid")
	if !ok || uid == "" {
		c.JSON(400, types.RestRespone{Message: "user_uid is required", Payload: nil})
		return
	}
	code, ok := c.GetQuery("code")
	if !ok || code == "" {
		c.JSON(400, types.RestRespone{Message: "code is required", Payload: nil})
		return
	}
	user, err := ctrl.Repository.Verify(uid, code, c.MustGet("token").(string))
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: user})
}

// @Summary Change User's Password
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param _ body types.InputChangeUserPass true "New password"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/change-password [post]
func (ctrl *UserController) ChangePassword(c *gin.Context) {

	var input types.InputChangeUserPass
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	if err := ctrl.Repository.ChangePassword(&input, c.MustGet("account_id").(int64), c.ClientIP()); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: nil})
}

// @Summary Get code for password change
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param _ body types.InputRequestChangePassword true "Email"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/request-change-password [post]
func (ctrl *UserController) GenerateAndSendChangePassCode(c *gin.Context) {

	var input types.InputRequestChangePassword
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	if err := ctrl.Repository.SendChangePassLink(c.MustGet("token").(string), &input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: nil})
}

// @Summary Send email verification link
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "User UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/{uid}/send-verify-link [get]
func (ctrl *UserController) SendEmailVerification(c *gin.Context) {

	if err := ctrl.Repository.SendEmailVerification(c.MustGet("uid").(string), c.MustGet("token").(string)); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: nil})
}

// @Summary Send email to user by uid
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param Token header string true "Account Token"
// @Param _ body types.InputSendEmailByEmail true "Email"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/send-email [post]
func (ctrl *UserController) SendEmail(c *gin.Context) {

	var input types.InputSendEmailByEmail
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	if err := ctrl.Repository.SendEmail(&input, c.MustGet("token").(string)); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: nil})
}

// @Summary List Users
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param Token header string true "Account Token"
// @Param _ body types.InputGetListUser true "Email"
// @Success 200 {object} []types.User
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/send-email [post]
func (ctrl *UserController) UserList(c *gin.Context) {

	var input types.InputGetListUser
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	data, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

// @Summary List accounts which contains the user
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	Authorization header string true "Email:Password"
// @Success 200 {object} []types.Account
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/account-choice [post]
func (ctrl *UserController) AccountChoice(c *gin.Context) {

	var input types.InputGinHeader
	if err := c.ShouldBindHeader(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	strArr := strings.Split(string(input.Authorization), " ")
	if len(strArr) != 2 {
		c.JSON(400, types.RestRespone{Message: "ERROR: Wrong login header!", Payload: nil})
		return
	}

	basicAuth, err := base64.StdEncoding.DecodeString(strArr[1])
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	strArr = strings.Split(string(basicAuth), ":")
	if len(strArr) != 2 {
		c.JSON(400, types.RestRespone{Message: "ERROR: Wrong login header!", Payload: nil})
		return
	}

	tokens, err := ctrl.Repository.AccountChoice(strArr[0], strArr[1], c.ClientIP())
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: tokens})
}

// @Summary Send SMS code
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param _ body types.InputGetPhone true "Phone"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/send-sms-code [post]
func (ctrl *UserController) SendPhoneConfirmSMSCode(c *gin.Context) {

	var input types.InputGetPhone
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	if err := ctrl.Repository.SendPhoneConfirmSMSCode(input.Phone, c.MustGet("token").(string), c.ClientIP()); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: nil})
}

// @Summary Login or Confirm User with phone and code
// @Tags Users
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param _ body types.InputGetPhoneAndCode true "Phone"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/users/sms-login [post]
func (ctrl *UserController) LoginSMSPhone(c *gin.Context) {

	var input types.InputGetPhoneAndCode
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	token, err := ctrl.Repository.LoginSMSPhone(input.Phone, input.Code, c.MustGet("token").(string), c.ClientIP())
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: token})
}
