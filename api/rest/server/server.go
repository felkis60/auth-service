package main

import (
	"log"
	"os"

	r "gitlab.com/felkis60/auth-service/api/rest/router"
	start "gitlab.com/felkis60/auth-service/init"
	l "gitlab.com/felkis60/auth-service/log"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware

	_ "gitlab.com/felkis60/auth-service/api/rest/server/docs"
)

func main() {
	start.SystemStartup(true, true)
	router := r.SetupRouter()
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	log.Printf(l.INFStartServer, "rest")
	router.Run(":" + os.Getenv("RESTPORT"))

}
