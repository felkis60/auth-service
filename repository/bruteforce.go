package repository

import (
	"log"
	"os"
	"strconv"
	"time"

	"gitlab.com/felkis60/auth-service/types"

	"gorm.io/gorm"
)

type BruteforceRepository struct {
	Db *gorm.DB
}

var BRUTEFORCE_CHECK_IS_ON = true
var BRUTEFORCE_TIME = "60 second"
var BRUTEFORCE_REQUESTS_AMOUNT = int(20)

func SetBruteforceVariablesFromEnv() {
	if tempString := os.Getenv("BRUTEFORCE_TIME"); tempString != "" {
		BRUTEFORCE_TIME = tempString + " second"
	}
	if tempString := os.Getenv("BRUTEFORCE_CHECK_IS_ON"); tempString != "" {
		if tempResult, err := strconv.ParseBool(tempString); err != nil {
			BRUTEFORCE_CHECK_IS_ON = tempResult
		}
	}
	if tempString := os.Getenv("BRUTEFORCE_REQUESTS_AMOUNT"); tempString != "" {
		if tempResult, err := strconv.ParseInt(tempString, 10, 64); err != nil {
			BRUTEFORCE_REQUESTS_AMOUNT = int(tempResult)
		}
	}
}

func (rep *BruteforceRepository) Create(input *types.BruteforceHistory) {
	input.CreatedAt = time.Time{}
	rep.Db.Create(input)
}

func (rep *BruteforceRepository) CheckBruteForce(user_uid string, clientIP string) {
	if BRUTEFORCE_CHECK_IS_ON {
		if clientIP != "" {
			var history []types.BruteforceHistory
			if result := rep.Db.Find(&history, "client_ip = ? AND success = false AND created_at > NOW() - ?::interval", clientIP, BRUTEFORCE_TIME); result.Error != nil {
				log.Print(result.Error)
			} else if len(history) >= BRUTEFORCE_REQUESTS_AMOUNT {
				rep.Db.Create(&types.BanList{IP: clientIP})
			}
		}
	}
}
