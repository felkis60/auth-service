package repository

import (
	"context"
	"errors"
	"log"
	"math"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	smsClient "gitlab.com/felkis60/auth-service/clients/sms-service"
	db "gitlab.com/felkis60/auth-service/database"
	logs "gitlab.com/felkis60/auth-service/log"
	tools "gitlab.com/felkis60/auth-service/tools"
	types "gitlab.com/felkis60/auth-service/types"
	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	smsApp "gitlab.com/felkis60/sms-service/api/grpc/app"

	"github.com/gofrs/uuid"
	"github.com/golang-jwt/jwt"
	"github.com/lib/pq"
	"gorm.io/gorm"
)

type UserRepository struct {
	Db *gorm.DB
}

func checkUser(data *types.User) error {
	if data.Email == nil && data.Phone == nil {
		return errors.New(logs.ERREmailOrPhoneReq)
	}
	if data.Phone != nil {
		if *data.Phone == "" {
			return errors.New("ERROR: phone cannot be ''")
		}
	}
	if data.Email != nil {
		// if !helpers.ValidEmail(*data.Email) {
		// 	return errors.New(logs.ERRBadEmail)
		// }
		if data.Password == "" {
			return errors.New(logs.ERRPassReq)
		}
	}
	if data.NewEmail != nil {
		// if !helpers.ValidEmail(*data.NewEmail) {
		// 	return errors.New(logs.ERRBadEmail)
		// }
	}
	if data.Nickname != nil {
		if *data.Nickname == "" {
			return errors.New("ERROR: nickname cannot be ''")
		}
	}
	return nil
}

func GenerateVerificationCodePhone(user *types.User) (string, error) {
	if user.Phone != nil {
		if user.PhoneVerify && user.NewPhone != nil && *user.NewPhone != "" {
			return helpers.GenerateMD5(strconv.FormatInt(user.AccountsID, 10) + user.Name + user.SecretToken + *user.NewPhone), nil
		}
		return helpers.GenerateMD5(strconv.FormatInt(user.AccountsID, 10) + user.Name + user.SecretToken + *user.Phone), nil
	}
	return "", errors.New("ERROR: user's phone is nil")
}

func GenerateVerificationCodeEmail(user *types.User) (string, error) {
	if user.Email != nil {
		if user.EmailVerify && user.NewEmail != nil && *user.NewEmail != "" {
			return helpers.GenerateMD5(strconv.FormatInt(user.AccountsID, 10) + user.Name + user.SecretToken + *user.NewEmail), nil
		}
		return helpers.GenerateMD5(strconv.FormatInt(user.AccountsID, 10) + user.Name + user.SecretToken + *user.Email), nil
	}
	return "", errors.New("ERROR: user's email is nil")
}

func GenerateChangePassCode(user *types.User) (string, error) {
	if user.Email != nil {
		return helpers.GenerateMD5(strconv.FormatInt(user.AccountsID, 10) + user.Name + user.SecretToken + user.Password + *user.Email), nil
	}
	return "", errors.New("ERROR: user's email is nil")
}

func (rep *UserRepository) writeAndCheckBaseUser(user *types.User, input *types.InputCreateEditUser, accountToken string) error {

	if input.ExtID != nil {
		if *input.ExtID == 0 {
			user.ExtID = nil
		} else {
			user.ExtID = input.ExtID
		}
	}

	if input.Nickname != nil {
		if *input.Nickname == "" {
			user.Nickname = nil
		} else {
			user.Nickname = input.Nickname
		}
	}

	if input.Name != nil {
		user.Name = *input.Name
	}

	if input.LastName != nil {
		user.LastName = *input.LastName
	}

	if user.Nickname != nil && *user.Nickname != "" {
		_, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "nickname", Value: *user.Nickname, NotTheID: &user.ID})
		if err == nil {
			return errors.New(logs.ERRUsersAlreadyExistsNickname)
		}
	}

	return nil
}

func (rep *UserRepository) writeAndCheckNewPhoneEmailUser(user *types.User, input *types.InputCreateEditUser, accountToken string) error {

	if (user.Phone != nil && user.PhoneVerify) || (user.Email != nil && user.EmailVerify) {
		if input.Phone != nil {
			var tempPhone = helpers.CutPhone(helpers.CleanPhone(*input.Phone))
			_, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "phone", Value: tempPhone, NotTheID: &user.ID})
			if err == nil {
				return errors.New(logs.ERRUsersAlreadyExistsPhone)
			}
			user.NewPhone = &tempPhone

		}
		if input.Email != nil {
			var tempEmail = helpers.EmailCleanup(*input.Email)
			_, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "email", Value: tempEmail, NotTheID: &user.ID})
			if err == nil {
				return errors.New(logs.ERRUsersAlreadyExistsEmail)
			}
			user.NewEmail = &tempEmail

		}
	}

	return nil
}

func (rep *UserRepository) Create(input *types.InputCreateEditUser, accountToken string, sendEmailSMSCode bool) (*types.User, error) {
	log.Println("UserRepository.Create")
	var accRep = AccountRepository{rep.Db}
	var tempAcc types.Account
	if err := accRep.Get(&tempAcc, accountToken); err != nil {
		return nil, err
	}
	if input.Email != nil && input.Phone != nil {
		if *input.Phone != "" && *input.Email != "" {
			return nil, errors.New("ERROR: only email or phone, not both")
		}
	}

	var data types.User

	data.AccountsID = tempAcc.ID

	if err := rep.writeAndCheckBaseUser(&data, input, accountToken); err != nil {
		return nil, err
	}

	if input.Phone != nil {
		var tempPhone = helpers.CutPhone(helpers.CleanPhone(*input.Phone))
		data.Phone = &tempPhone
	}

	if input.Email != nil {
		var tempEmail = helpers.EmailCleanup(*input.Email)
		data.Email = &tempEmail
	}

	if input.Password != nil {
		data.Password = helpers.GenerateMD5(*input.Password)
	}

	if err := checkUser(&data); err != nil {
		return nil, err
	}

	//Check base for uniqnes
	if data.Email != nil {
		log.Println("UserRepository.Create data.Email != nil")
		existingUser, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "email", Value: *data.Email})
		if err == nil {
			log.Println("UserRepository.Create data.Email != nil err == nil")
			if existingUser.EmailVerify || existingUser.PhoneVerify {
				return nil, errors.New(logs.ERRUsersAlreadyExistsEmail)
			} else {

				if input.Password != nil {
					existingUser.Password = helpers.GenerateMD5(*input.Password)
				}
				if err := rep.writeAndCheckBaseUser(existingUser, input, accountToken); err != nil {
					return nil, err
				}
				if err := checkUser(existingUser); err != nil {
					return nil, err
				}

				if err := rep.SendEmailVerification(existingUser.UID, accountToken); err != nil {
					return nil, err
				}
				existingUser.SecretTokenForView = existingUser.SecretToken
				return existingUser, nil
			}
		} else {
			log.Println("UserRepository.Create data.Email != nil err != nil")
		}
	}

	if data.Phone != nil {
		existingUser, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "phone", Value: *data.Phone})
		if err == nil {
			if existingUser.EmailVerify || existingUser.PhoneVerify {
				return nil, errors.New(logs.ERRUsersAlreadyExistsPhone)
			} else {

				if input.Password != nil {
					existingUser.Password = helpers.GenerateMD5(*input.Password)
				}
				if err := rep.writeAndCheckBaseUser(existingUser, input, accountToken); err != nil {
					return nil, err
				}
				if err := checkUser(existingUser); err != nil {
					return nil, err
				}

				if err := rep.SendEmailVerification(existingUser.UID, accountToken); err != nil {
					return nil, err
				}
				existingUser.SecretTokenForView = existingUser.SecretToken
				return existingUser, nil
			}
		}
	}

	data.SecretToken = helpers.RandString(32)
	data.SecretTokenForView = data.SecretToken
	data.UID = uuid.Must(uuid.NewV4()).String()
	data.PhoneVerify = false
	data.EmailVerify = false

	if result := rep.Db.Create(&data); result.Error != nil {
		return nil, result.Error
	}

	code, err := GenerateVerificationCodeEmail(&data)
	if err != nil {
		return nil, err
	}

	data.CodeToVerify = code

	if sendEmailSMSCode {
		if data.Email != nil {
			if err := rep.SendEmailVerification(data.UID, accountToken); err != nil {
				log.Print(err)
			}
		} else if data.Phone != nil {
			var redactedPhone = *data.Phone
			if len(redactedPhone) < 11 {
				redactedPhone = "7" + redactedPhone
			}
			if err := rep.SendPhoneConfirmSMSCode(redactedPhone, accountToken, ""); err != nil {
				log.Print(err)
			}
		}
	}

	return &data, nil

}

func (rep *UserRepository) SendEmailVerification(usersUID string, accountToken string) error {
	log.Println("SendEmailVerification")
	user, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "uid", Value: usersUID})
	if err != nil {
		return err
	}

	if user.Email == nil && user.NewEmail == nil {
		return errors.New("ERROR: email is nil")
	}

	var accRep = AccountRepository{Db: rep.Db}
	account, err := accRep.GetByID(user.AccountsID)
	if err != nil {
		return err
	}

	varificationCode, err := GenerateVerificationCodeEmail(user)
	if err != nil {
		return err
	}

	params := url.Values{}
	params.Add("user_uid", user.UID)
	params.Add("code", varificationCode)

	var emailData = types.EmailData{From: account.DialerEmail, Text: "?" + params.Encode()}
	if user.EmailVerify && user.NewEmail != nil {
		emailData.To = *user.NewEmail
	} else if user.Email != nil {
		emailData.To = *user.Email
	}
	log.Println("SendEmailVerification emailData.To=" + emailData.To)
	log.Println("SendEmailVerification emailData.From=" + emailData.From)
	if err := tools.SendEmailVerifyEmail(account, &emailData); err != nil {
		return err
	}

	return nil
}

func (rep *UserRepository) SendChangePassLink(accountToken string, input *types.InputRequestChangePassword) error {

	input.Email = helpers.EmailCleanup(input.Email)
	if !helpers.ValidEmail(input.Email) {
		return errors.New(logs.ERRBadEmail)
	}

	var user, err = rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "email", Value: input.Email, Verified: true})
	if err != nil {
		return err
	}
	if !user.EmailVerify {
		return errors.New(logs.ERRNoSuchUser)
	}

	var accRep = AccountRepository{Db: rep.Db}
	var account types.Account
	err = accRep.Get(&account, accountToken)
	if err != nil {
		return err
	}

	changePassCode, err := GenerateChangePassCode(user)
	if err != nil {
		return err
	}

	params := url.Values{}
	params.Add("user_uid", user.UID)
	params.Add("code", changePassCode)
	var emailData = types.EmailData{To: *user.Email, From: account.DialerEmail, Text: "?" + params.Encode()}
	if err := tools.SendEmailChangePass(&account, &emailData); err != nil {
		return err
	}

	return nil
}

func (rep *UserRepository) Edit(uid string, input *types.InputCreateEditUser, accountToken string) (*types.User, error) {
	//Change password with another method
	input.Password = nil

	//Checking user
	// if input.JWTToken == nil {
	// 	return nil, errors.New("ERROR: must be logged to edit")
	// }

	// tokenData, err := rep.CheckToken(accountToken, *input.JWTToken)
	// if err != nil {
	// 	return nil, err
	// }

	data, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "uid", Value: uid})
	if err != nil {
		return nil, err
	}

	// if int64((*tokenData)["user_id"].(float64)) != data.ID {
	// 	return nil, errors.New("ERROR: cannot change other users")
	// }

	//Editing
	if input.EmailVerify != nil {
		data.EmailVerify = *input.EmailVerify
	}

	if err := rep.writeAndCheckBaseUser(data, input, accountToken); err != nil {
		return nil, err
	}
	if err := rep.writeAndCheckNewPhoneEmailUser(data, input, accountToken); err != nil {
		return nil, err
	}

	//Checking edit
	if err := checkUser(data); err != nil {
		return nil, err
	}

	if data.NewEmail != nil && *data.NewEmail != "" {
		if err := rep.SendEmailVerification(data.UID, accountToken); err != nil {
			log.Print(err)
		}
	}

	if data.NewPhone != nil && *data.NewPhone != "" {
		if err = rep.SendPhoneConfirmSMSCode(*data.NewPhone, accountToken, ""); err != nil {
			log.Print(err)
		}
	}

	if result := rep.Db.Save(data); result.Error != nil {
		return nil, result.Error
	}

	return data, nil
}

func (rep *UserRepository) verifyEmail(data *types.User) {
	if data.EmailVerify && data.NewEmail != nil && *data.NewEmail != "" {
		data.Email = data.NewEmail
		data.NewEmail = nil
	}
	data.EmailVerify = true
	rep.Db.Save(data)
}

func (rep *UserRepository) verifyPhone(data *types.User) {
	if data.PhoneVerify && data.NewPhone != nil && *data.NewPhone != "" {
		data.Email = data.NewPhone
		data.NewPhone = nil
	}
	data.PhoneVerify = true
	rep.Db.Save(data)
}

func (rep *UserRepository) Delete(input *types.InputDeleteUser) (*types.User, error) {

	data, err := rep.Get(&types.GetOneUser{AccountToken: input.AccountToken, By: "uid", Value: input.UID})
	if err != nil {
		return nil, err
	}

	if !data.EmailVerify {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	if helpers.GenerateMD5(input.Password) != data.Password {
		return nil, errors.New(logs.ERRWrongPass)
	}

	rep.Db.Delete(data)

	return data, nil

}

func (rep *UserRepository) CheckToken(accountToken string, tokenString string) (*jwt.MapClaims, error) {

	tokenData, err := tools.TokenValidAndGetData(tokenString, "ACCESS_SECRET", accountToken)
	if err != nil {
		return nil, err
	}

	return tokenData, nil

}

func (rep *UserRepository) RefreshToken(accountToken string, tokenString string) (*types.JWTToken, error) {

	tokenData, err := tools.TokenValidAndGetData(tokenString, "REFRESH_SECRET", accountToken)
	if err != nil {
		return nil, err
	}

	user, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "id", Value: (*tokenData)["user_id"]})
	if err != nil {
		return nil, err
	}

	if !user.EmailVerify && !user.PhoneVerify {
		return nil, errors.New(logs.ERRBadJWTToken)
	}

	var accRep = AccountRepository{Db: rep.Db}
	account, err := accRep.GetByID(user.AccountsID)
	if err != nil {
		return nil, err
	}

	token, err := tools.CreateJWTToken(accountToken, user, account.AccessTokenTime, account.RefreshTokenTime)
	if err != nil {
		return nil, err
	}

	return token, nil
}

func (rep *UserRepository) SendPhoneConfirmSMSCode(phone string, accountToken string, clientIP string) error {
	phone = helpers.CutPhone(helpers.CleanPhone(phone))
	var loginHistRep = BruteforceRepository{rep.Db}
	var hist = types.BruteforceHistory{Data: phone, ClientIP: clientIP, Success: false}
	defer loginHistRep.Create(&hist)

	loginHistRep.CheckBruteForce(phone, clientIP)

	var accRep = AccountRepository{rep.Db}
	var tempAcc types.Account
	if err := accRep.Get(&tempAcc, accountToken); err != nil {
		return err
	}

	userData, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "phone", Value: phone})
	if err != nil {
		if err.Error() == logs.ERRNoSuchUser {
			if userData, err = rep.Create(&types.InputCreateEditUser{Phone: &phone}, accountToken, false); err != nil {
				return errors.New("ERROR: no user to confirm and can't create one: " + err.Error())
			}
		} else {
			return err
		}
	}

	if db.RedisDB == nil {
		return errors.New("ERROR: redis is not inited, can't store code")
	}
	if userData == nil {
		return errors.New("user is nil")
	}

	var randNumber, _ = helpers.GetRandNum(6)

	if result := db.RedisDB.Set(db.RedisContext, tools.GenerateRedisKeyForSMSCode(accountToken, userData.UID), randNumber, time.Second*time.Duration(tempAcc.SMSCodeTimeToLive)); result.Err() != nil {
		return result.Err()
	}

	var redactedPhone = phone
	if len(redactedPhone) < 11 {
		redactedPhone = "7" + redactedPhone
	}

	_, err = smsClient.SMSClient.SendSMS(context.Background(), &smsApp.SMSSend{From: "Auth", Text: "Ваш код: " + randNumber, PhoneNumber: redactedPhone, WalletToken: os.Getenv("SMS_WALLET_TOKEN")})
	if err != nil {
		db.RedisDB.Del(db.RedisContext, tools.GenerateRedisKeyForSMSCode(accountToken, userData.UID))
		return err
	}

	log.Print(2)
	hist.Success = true
	return nil
}

func (rep *UserRepository) LoginSMSPhone(phone string, smsCode string, accountToken string, clientIP string) (*types.JWTToken, error) {
	phone = helpers.CutPhone(helpers.CleanPhone(phone))
	var loginHistRep = BruteforceRepository{rep.Db}
	var hist = types.BruteforceHistory{Data: phone, ClientIP: clientIP, Success: false}
	defer loginHistRep.Create(&hist)

	loginHistRep.CheckBruteForce(phone, clientIP)

	if phone == "" {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	user, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "phone", Value: phone})
	if err != nil {
		return nil, err
	}

	var accRep = AccountRepository{Db: rep.Db}
	account, err := accRep.GetByID(user.AccountsID)
	if err != nil {
		return nil, err
	}

	if db.RedisDB == nil {
		return nil, errors.New("ERROR: redis is not inited, can't check the code")
	}

	storedCode, err := db.RedisDB.Get(db.RedisContext, tools.GenerateRedisKeyForSMSCode(accountToken, user.UID)).Result()
	if err != nil {
		return nil, err
	}

	if storedCode == "" || storedCode != smsCode {
		return nil, errors.New("ERROR: Bad SMS code")
	}

	if !user.PhoneVerify {
		user.PhoneVerify = true
		if result := rep.Db.Save(&user); result.Error != nil {
			return nil, result.Error
		}
	}

	token, err := tools.CreateJWTToken(accountToken, user, account.AccessTokenTime, account.RefreshTokenTime)
	if err != nil {
		return nil, err
	}

	db.RedisDB.Del(db.RedisContext, tools.GenerateRedisKeyForSMSCode(accountToken, user.UID))

	hist.Success = true
	return token, nil

}

func (rep *UserRepository) LoginPhone(phone string, password string, accountToken string, clientIP string) (*types.JWTToken, error) {
	phone = helpers.CutPhone(helpers.CleanPhone(phone))
	var loginHistRep = BruteforceRepository{rep.Db}
	var hist = types.BruteforceHistory{Data: phone, ClientIP: clientIP, Success: false}
	defer loginHistRep.Create(&hist)

	loginHistRep.CheckBruteForce(phone, clientIP)

	if phone == "" {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	var user types.User
	if result := db.DbAccountLeftJoin("users", accountToken, rep.Db).First(&user, "phone = ? AND password = ? AND phone_verify = ?", phone, helpers.GenerateMD5(password), true); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	if !user.PhoneVerify {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	var accRep = AccountRepository{Db: rep.Db}
	account, err := accRep.GetByID(user.AccountsID)
	if err != nil {
		return nil, err
	}

	token, err := tools.CreateJWTToken(accountToken, &user, account.AccessTokenTime, account.RefreshTokenTime)
	if err != nil {
		return nil, err
	}

	hist.Success = true
	return token, nil
}

func (rep *UserRepository) LoginEmail(email string, password string, accountToken string, clientIP string) (*types.JWTToken, error) {

	var loginHistRep = BruteforceRepository{rep.Db}
	var hist = types.BruteforceHistory{Data: email, ClientIP: clientIP, Success: false}
	defer loginHistRep.Create(&hist)

	loginHistRep.CheckBruteForce(email, clientIP)

	if email == "" {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	var user types.User
	if result := db.DbAccountLeftJoin("users", accountToken, rep.Db).Find(&user, "email = ? AND password = ? AND email_verify = TRUE", email, helpers.GenerateMD5(password)); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	if !user.EmailVerify {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	var accRep = AccountRepository{Db: rep.Db}
	account, err := accRep.GetByID(user.AccountsID)
	if err != nil {
		return nil, err
	}

	token, err := tools.CreateJWTToken(accountToken, &user, account.AccessTokenTime, account.RefreshTokenTime)
	if err != nil {
		return nil, err
	}

	hist.Success = true
	return token, nil
}

func (rep *UserRepository) LoginNickname(nickname string, password string, accountToken string, clientIP string) (*types.JWTToken, error) {

	var loginHistRep = BruteforceRepository{rep.Db}
	var hist = types.BruteforceHistory{Data: nickname, ClientIP: clientIP, Success: false}
	defer loginHistRep.Create(&hist)

	loginHistRep.CheckBruteForce(nickname, clientIP)

	if nickname == "" {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	var user types.User
	if result := db.DbAccountLeftJoin("users", accountToken, rep.Db).First(&user, "nickname = ? AND password = ? AND (email_verify = ? OR phone_verify = ?)", nickname, helpers.GenerateMD5(password), true, true); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	if !user.EmailVerify && !user.PhoneVerify {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	var accRep = AccountRepository{Db: rep.Db}
	account, err := accRep.GetByID(user.AccountsID)
	if err != nil {
		return nil, err
	}

	token, err := tools.CreateJWTToken(accountToken, &user, account.AccessTokenTime, account.RefreshTokenTime)
	if err != nil {
		return nil, err
	}

	hist.Success = true
	return token, nil
}

func (rep *UserRepository) DoWithSecretToken(md5sum, email string, clientIP string) (*types.JWTToken, error) {

	var loginHistRep = BruteforceRepository{rep.Db}
	var hist = types.BruteforceHistory{Data: email, ClientIP: clientIP, Success: false}
	defer loginHistRep.Create(&hist)
	loginHistRep.CheckBruteForce(email, clientIP)

	var data types.User
	if result := rep.Db.Model(&types.User{}).Limit(1).Find(&data, "users.email = ?", email); result.Error != nil {
		return nil, result.Error
	} else if result.RowsAffected <= 0 {
		return nil, errors.New("ERROR: No such user")
	}

	if data.ID <= 0 {
		return nil, errors.New("ERROR: No such user")
	}

	if data.Email != nil {
		if helpers.GenerateMD5(data.UID+*data.Email+data.SecretToken) != md5sum {
			return nil, errors.New("ERROR: No such user")
		}
	} else {
		return nil, errors.New("ERROR: No such user")
	}

	var accRep = AccountRepository{Db: rep.Db}
	account, err := accRep.GetByID(data.AccountsID)
	if err != nil {
		return nil, err
	}

	token, err := tools.CreateJWTToken(account.Token, &data, account.AccessTokenTime, account.RefreshTokenTime)
	if err != nil {
		return nil, err
	}

	hist.Success = true
	return token, nil
}

func (rep *UserRepository) GetSecretForPosiblyNewUserByPhone(accountToken string, phone string, clientIP string) (string, error) {

	phone = helpers.CutPhone(helpers.CleanPhone(phone))
	var loginHistRep = BruteforceRepository{rep.Db}
	var hist = types.BruteforceHistory{Data: phone, ClientIP: clientIP, Success: false}
	defer loginHistRep.Create(&hist)

	loginHistRep.CheckBruteForce(phone, clientIP)

	var accRep = AccountRepository{rep.Db}
	var tempAcc types.Account
	if err := accRep.Get(&tempAcc, accountToken); err != nil {
		return "", err
	}

	userData, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "phone", Value: phone})
	if err != nil {
		if err.Error() == logs.ERRNoSuchUser {
			if userData, err = rep.Create(&types.InputCreateEditUser{Phone: &phone}, accountToken, false); err != nil {
				return "", errors.New("ERROR: no user to generate link and can't create one: " + err.Error())
			}
		} else {
			return "", err
		}
	}

	return userData.SecretToken, nil

}

func (rep *UserRepository) LoginWithPhoneAndSecret(accountToken string, phone string, secret string, clientIP string) (*types.JWTToken, error) {
	phone = helpers.CutPhone(helpers.CleanPhone(phone))
	var loginHistRep = BruteforceRepository{rep.Db}
	var hist = types.BruteforceHistory{Data: phone, ClientIP: clientIP, Success: false}
	defer loginHistRep.Create(&hist)

	loginHistRep.CheckBruteForce(phone, clientIP)

	if phone == "" {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	var user types.User
	if result := db.DbAccountLeftJoin("users", accountToken, rep.Db.Model(&types.User{})).First(&user, "phone = ? AND secret_token = ?", phone, secret); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	var accRep = AccountRepository{Db: rep.Db}
	account, err := accRep.GetByID(user.AccountsID)
	if err != nil {
		return nil, err
	}

	token, err := tools.CreateJWTToken(accountToken, &user, account.AccessTokenTime, account.RefreshTokenTime)
	if err != nil {
		return nil, err
	}

	user.SecretToken = helpers.RandString(32)
	if result := rep.Db.Save(&user); result.Error != nil {
		return nil, errors.New("new secret not saved")
	}

	hist.Success = true
	return token, nil
}

func RefreshSecretToken(uid, accountToken string, clientIP string) error {

	return nil
}

func (rep *UserRepository) Verify(userUID string, verificationCode string, accountToken string) (*types.User, error) {

	if verificationCode == "" {
		return nil, errors.New("ERROR: code cannot be empty")
	}

	user, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "uid", Value: userUID, SetSecretToken: true})
	if err != nil {
		return nil, err
	}

	if code, err := GenerateVerificationCodeEmail(user); code == verificationCode && err == nil {
		rep.verifyEmail(user)
		return user, nil
	}
	if code, err := GenerateVerificationCodePhone(user); code == verificationCode && err == nil {
		rep.verifyPhone(user)
		return user, nil
	}

	return nil, errors.New(logs.ERRInvalidVerify)

}

func (rep *UserRepository) ChangePassword(input *types.InputChangeUserPass, accountID int64, clientIP string) error {

	if input.Code == "" {
		return errors.New("'code' is required")
	}
	if input.UserUID == "" {
		return errors.New("'user_uid' is required")
	}

	var loginHistRep = BruteforceRepository{rep.Db}
	var hist = types.BruteforceHistory{Data: input.UserUID, ClientIP: clientIP, Success: false}
	defer loginHistRep.Create(&hist)

	loginHistRep.CheckBruteForce(input.UserUID, clientIP)

	var accRep = AccountRepository{Db: rep.Db}
	account, err := accRep.GetByID(accountID)
	if err != nil {
		return err
	}

	user, err := rep.Get(&types.GetOneUser{AccountToken: account.Token, By: "uid", Value: input.UserUID})
	if err != nil {
		return err
	}

	if !user.EmailVerify {
		return errors.New(logs.ERRNoSuchUser)
	}
	if code, err := GenerateChangePassCode(user); code == input.Code && err == nil {
		if input.NewPassword == "" {
			return errors.New(logs.ERRPassReq)
		}

		if account.OldPasswordCheck {
			if input.OldPassword == nil {
				return errors.New(logs.ERROldPassReq)
			}
			if helpers.GenerateMD5(*input.OldPassword) != user.Password {
				return errors.New(logs.ERRWrongOldPass)
			}
		}

		user.Password = helpers.GenerateMD5(input.NewPassword)
		rep.Db.Save(&user)
		hist.Success = true
		return nil
	}

	return errors.New(logs.ERRInvalidVerify)

}

func (rep *UserRepository) SendEmail(input *types.InputSendEmailByEmail, accountToken string) error {

	if input.Email == "" {
		return errors.New(logs.ERRBadEmail)
	}
	input.Email = helpers.EmailCleanup(input.Email)

	var account types.Account
	if result := rep.Db.First(&account, "token = ?", accountToken); result.RowsAffected == 0 {
		return errors.New(logs.ERR)
	}

	user, err := rep.Get(&types.GetOneUser{AccountToken: accountToken, By: "email", Value: input.Email})
	if err != nil {
		return err
	}
	if user.Email != nil {
		var emailData = types.EmailData{To: *user.Email, From: account.DialerEmail, Title: input.Title, Text: input.Content}
		if err := tools.SendEmail(&account, &emailData); err != nil {
			return err
		}
	} else {
		return errors.New("ERROR: user's email is nil")
	}

	return nil
}

func (rep *UserRepository) Get(input *types.GetOneUser) (*types.User, error) {

	var tx = db.DbAccountLeftJoin("users", input.AccountToken, rep.Db.Model(&types.User{}))

	if input.NotTheID != nil {
		tx = tx.Where("users.id != ?", *input.NotTheID)
	}

	switch strings.ToLower(input.By) {
	case "id":
		tx = tx.Where("users.id = ?", input.Value)

	case "uid":
		tx = tx.Where("users.uid = ?", input.Value)

	case "email", "mail":
		if input.Verified {
			tx = tx.Where("users.email_verify = true")
		} else if input.NotVerified {
			tx = tx.Where("users.email_verify = false")
		}
		tx = tx.Where("users.email = ?", input.Value)

	case "phone", "number":
		if input.Verified {
			tx = tx.Where("users.phone_verify = true")
		} else if input.NotVerified {
			tx = tx.Where("users.phone_verify = false")
		}
		tx = tx.Where("users.phone = ?", input.Value)

	case "nickname", "nick":
		if input.Verified {
			tx = tx.Where("users.email_verify = true")
		} else if input.NotVerified {
			tx = tx.Where("users.email_verify = false")
		}
		tx = tx.Where("users.nickname = ?", input.Value)

	default:
		return nil, errors.New("ERROR: no such 'by' category")
	}

	var data types.User
	if result := tx.Limit(1).Find(&data, "users.deleted_at IS NULL"); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	if input.SetSecretToken {
		data.SecretTokenForView = data.SecretToken
	}

	return &data, nil
}

func (rep *UserRepository) GetAllByID(accountToken string, input *types.InputGetAllUsersByID) (*map[int64]*types.User, error) {
	var data []types.User
	if result := db.DbAccountLeftJoin("users", accountToken, rep.Db).Where("users.id = ANY(?)", pq.Array(input.IDs)).Find(&data); result.Error != nil {
		return nil, result.Error
	}

	var tempMap = make(map[int64]*types.User)
	for i := range data {
		tempMap[data[i].ID] = &data[i]
	}
	return &tempMap, nil
}

func (rep *UserRepository) List(accountToken string, input *types.InputGetListUser) (*types.Pagination, error) {

	var tx = db.DbAccountLeftJoin("users", accountToken, rep.Db.Model(&types.User{}))

	if input.Filters.IDs != nil {
		tx = tx.Where("users.id IN (?)", *input.Filters.IDs)
	}

	if input.Filters.UIDs != nil {
		tx = tx.Where("users.uid IN (?)", *input.Filters.UIDs)
	}

	if input.Filters.Emails != nil {
		tx = tx.Where("users.email IN (?)", *input.Filters.Emails)
	}

	var tempCount int64
	tx.Count(&tempCount)

	var tempOrder = ""
	if input.OrderBy != "" {
		tempOrder = "users" + "." + input.OrderBy
	}

	if input.OrderDir != "" {
		tempOrder += " " + input.OrderDir
	}

	var tx2 = db.DbAccountLeftJoin("users", accountToken, rep.Db.Model(&types.User{}))

	if input.Filters.IDs != nil {
		tx2 = tx2.Where("users.id IN (?)", *input.Filters.IDs)
	}
	if input.Filters.UIDs != nil {
		tx2 = tx2.Where("users.uid IN (?)", *input.Filters.UIDs)
	}
	if input.Filters.Emails != nil {
		tx2 = tx2.Where("users.email IN (?)", *input.Filters.Emails)
	}

	if tempOrder != "" {
		tx2 = tx2.Order(tempOrder)
	}

	var data []types.User
	if result := db.PaginateManual(&input.Page, &input.PageSize, 5, 0)(tx2).Scan(&data); result.Error != nil {
		return nil, result.Error
	}

	var tempPages = int(math.Ceil(float64(tempCount) / float64(input.PageSize)))

	return &types.Pagination{Page: &input.Page, Items: &data, TotalItems: &tempCount, TotalPages: &tempPages}, nil
}

func (rep *UserRepository) AccountChoice(email string, password string, clientIP string) (*[]types.Account, error) {

	if email == "" {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	var data []types.User
	if result := rep.Db.Find(&data, "email = ? AND password = ? AND email_verify = TRUE", email, helpers.GenerateMD5(password)); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchUser)
	}

	var accountsIDs = make(map[int64]bool)
	for i := range data {
		if data[i].EmailVerify {
			accountsIDs[data[i].AccountsID] = true
		}
	}

	var keys = make([]int64, len(accountsIDs))
	i := 0
	for k := range accountsIDs {
		keys[i] = k
		i++
	}

	var accounts []types.Account
	rep.Db.Table("accounts").Select("accounts.id, accounts.token, accounts.name").Find(&accounts, "accounts.id IN (?)", keys)

	return &accounts, nil
}
