package repository

import (
	"errors"

	logs "gitlab.com/felkis60/auth-service/log"
	types "gitlab.com/felkis60/auth-service/types"

	"gorm.io/gorm"
)

type AccountRepository struct {
	Db *gorm.DB
}

func checkAccount(data *types.Account) error {
	if data.Name == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.Token == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.AccessTokenTime == 0 {
		return errors.New(logs.ERRAllReq)
	}
	if data.RefreshTokenTime == 0 {
		return errors.New(logs.ERRAllReq)
	}
	if data.Domain == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.VerifyEmailEndpoint == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.ChangePasswordEndpoint == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.DialerHost == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.DialerPort == 0 {
		return errors.New(logs.ERRAllReq)
	}
	if data.DialerEmail == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.DialerPassword == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.DialerFromMask == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.VerifyMailBody == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.ChangePasswordMailBody == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.VerifyMailTitle == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.ChangePasswordMailTitle == "" {
		return errors.New(logs.ERRAllReq)
	}
	if data.SMSCodeTimeToLive <= 0 {
		data.SMSCodeTimeToLive = 60
	}
	return nil
}

func writeAccount(account *types.Account, input *types.InputCreateEditAccount) error {
	if input.Name != nil {
		account.Name = *input.Name
	}
	if input.Token != nil {
		account.Token = *input.Token
	}
	if input.AccessTokenTime != nil {
		var tempInt int = int(*input.AccessTokenTime)
		account.AccessTokenTime = tempInt
	}
	if input.RefreshTokenTime != nil {
		var tempInt int = int(*input.RefreshTokenTime)
		account.RefreshTokenTime = tempInt
	}
	if input.Domain != nil {
		account.Domain = *input.Domain
	}
	if input.VerifyEmailEndpoint != nil {
		account.VerifyEmailEndpoint = *input.VerifyEmailEndpoint
	}
	if input.ChangePasswordEndpoint != nil {
		account.ChangePasswordEndpoint = *input.ChangePasswordEndpoint
	}
	if input.DialerHost != nil {
		account.DialerHost = *input.DialerHost
	}
	if input.DialerPort != nil {
		var tempInt int = int(*input.DialerPort)
		account.DialerPort = tempInt
	}
	if input.DialerEmail != nil {
		account.DialerEmail = *input.DialerEmail
	}
	if input.DialerPassword != nil {
		account.DialerPassword = *input.DialerPassword
	}
	if input.DialerFromMask != nil {
		account.DialerFromMask = *input.DialerFromMask
	}
	if input.VerifyMailBody != nil {
		account.VerifyMailBody = *input.VerifyMailBody
	}
	if input.ChangePasswordMailBody != nil {
		account.ChangePasswordMailBody = *input.ChangePasswordMailBody
	}
	if input.VerifyMailTitle != nil {
		account.VerifyMailTitle = *input.VerifyMailTitle
	}
	if input.ChangePasswordMailTitle != nil {
		account.ChangePasswordMailTitle = *input.ChangePasswordMailTitle
	}
	if input.OldPasswordCheck != nil {
		account.OldPasswordCheck = *input.OldPasswordCheck
	}
	if input.SMSCodeTimeToLive != nil {
		account.SMSCodeTimeToLive = *input.SMSCodeTimeToLive
	}

	//Checks
	if err := checkAccount(account); err != nil {
		return err
	}
	return nil
}

func (rep *AccountRepository) Create(data *types.Account, input *types.InputCreateEditAccount) error {

	if err := writeAccount(data, input); err != nil {
		return err
	}

	var account types.Account
	if result := rep.Db.First(&account, "token = ?", data.Token); result.RowsAffected != 0 {
		return errors.New(logs.ERRTokenIsOccupied)
	}

	rep.Db.Create(data)

	return nil
}

func (rep *AccountRepository) Edit(data *types.Account, input *types.InputCreateEditAccount, token string) error {

	if err := rep.Get(data, token); err != nil {
		return err
	}

	if input.Token != nil && *input.Token != data.Token {
		var account types.Account
		if result := rep.Db.First(&account, "token = ?", input.Token); result.RowsAffected != 0 {
			return errors.New(logs.ERRTokenIsOccupied)
		}
	}

	writeAccount(data, input)

	rep.Db.Save(data)
	return nil
}

func (rep *AccountRepository) Get(data *types.Account, token string) error {
	if result := rep.Db.First(data, "token = ?", token); result.RowsAffected == 0 {
		return errors.New(logs.ERRNoSuchToken)
	}
	return nil
}

func (rep *AccountRepository) GetByID(id int64) (*types.Account, error) {
	var data types.Account
	if result := rep.Db.Limit(1).Find(&data, id); result.Error != nil {
		return nil, result.Error
	} else if result.RowsAffected == 0 {
		return nil, errors.New("ERROR: no such account")
	}
	return &data, nil
}
