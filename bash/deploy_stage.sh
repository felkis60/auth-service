#!/bin/bash

docker build -f dockerfile.rest -t auth-service-rest_stage . --network=host
docker build -f dockerfile.grpc -t auth-service-grpc_stage . --network=host
docker stop auth-service-rest_stage || true
docker stop auth-service-grpc_stage || true
docker rm auth-service-rest_stage || true
docker rm auth-service-grpc_stage || true
docker run --network host -d -p 6310:6310 -v `pwd`:/srv/app --name auth-service-rest_stage auth-service-rest_stage
docker run --network host -d -p 6311:6311 -v `pwd`:/srv/app --name auth-service-grpc_stage auth-service-grpc_stage