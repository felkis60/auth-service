#!/bin/bash

docker build -f dockerfile.rest -t auth-service-rest . --network=host
docker build -f dockerfile.grpc -t auth-service-grpc . --network=host
docker stop auth-service-rest || true
docker stop auth-service-grpc || true
docker rm auth-service-rest || true
docker rm auth-service-grpc || true
docker run --network host -d -p 6300:6300 -v `pwd`:/srv/app --name auth-service-rest auth-service-rest
docker run --network host -d -p 6301:6301 -v `pwd`:/srv/app --name auth-service-grpc auth-service-grpc