package main

import (
	"strings"

	"gitlab.com/felkis60/auth-service/database"
	startup "gitlab.com/felkis60/auth-service/init"
	"gitlab.com/felkis60/auth-service/repository"
	"gitlab.com/felkis60/auth-service/tools"
	"gitlab.com/felkis60/auth-service/types"
)

// @title Auth Service
// @version 0.1

func main() {
	//accountController := controllers.AccountController{}
	//userController := controllers.UserController{}

	startup.SystemStartup(true, true)

	var accRepo = repository.AccountRepository{Db: database.Db}

	var account types.Account
	accRepo.Get(&account, "nGTZqTmXvhRMZ9S8")

	var allUsers []types.User
	database.Db.Model(&types.User{}).Where("users.accounts_id = ?", account.ID).Find(&allUsers)

	for i := range allUsers {
		if allUsers[i].Email == nil {
			continue
		}
		if *allUsers[i].Email == "" {
			continue
		}
		if !allUsers[i].EmailVerify {
			continue
		}

		tools.SendEmail(&account, &types.EmailData{To: strings.TrimSpace(*allUsers[i].Email), Title: "Получи тонну знаний о музыкальной индустрии!", Text: "<html><body>Меня зовут Влад и я сделал проект SFEROOM! <br><br>Все начиналось с того, что я писал музыку и хотел продвигать себя как артиста.<br><br>Ну судьба закрутилась так, что фокус внимания сместился на других исполнителей, на их упаковку и продвижение. Некоторые подписчики этого паблика, помнят времена с чего начинался сферум.<br><br>Я видел как строиться и меняется рынок на протяжении десяти лет. Я внедрял инновационные подходы и методы продвижения и сделал с нуля несколько больших звезд.<br><br>Я работал на чужие мечты. Сейчас пришло время осуществить свою. <br><br>Со всем своим богажом знаний и опытом, я буду перезапускать свой музыкальный проект.<br><br>Делать это я буду вместе с ИСАЙЯ - это человек с которым мы знакомы 10 лет и вместе стояли у истоков становления цифрового рынка.<br><br>Я сделал телеграмм канал в котором буду давать очень полезный и наглядный контент, как делать маркетинг и работать с аудиторией.<br><br>А чтобы бы было еще интереснее, первые 20 человек, которые напишут коммент в тг под постом, который я опубликую 17 апреля в 17:17, получат по 5000 рублей на карту!<br><br>А пост то будет не простой, в комментарии надо будет скинуть ссылку на любую свою работу, мы выберем одного человека и спродюсируем ему релиз, вместе с ребятами из команды NLO.<br><br>Со Степой мы знакомы около 7 лет и он жестко прохавал кухню изнутри, в итоге пришел к успеху и делает гениальные работы.<br><br>Вообщем, будет очень интересно!<br>https://t.me/sferoom</html></body>"})
	}

	//userController.SendEmail()

}
