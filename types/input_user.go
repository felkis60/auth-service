package types

type InputCreateEditUser struct {
	ExtID       *int64  `json:"ext_id"`
	Nickname    *string `json:"nickname"`
	Password    *string `json:"password"`
	Name        *string `json:"name"`
	LastName    *string `json:"last_name"`
	Phone       *string `json:"phone"`
	Email       *string `json:"email"`
	EmailVerify *bool   `json:"email_verify"`
	JWTToken    *string `json:"-"`
}

type GetOneUser struct {
	AccountToken   string      `json:"-"`
	SetSecretToken bool        `json:"-"`
	NotTheID       *int64      `json:"-"`
	Verified       bool        `json:"verified"`
	NotVerified    bool        `json:"not_verified"`
	By             string      `json:"by"`
	Value          interface{} `json:"value"`
}

type InputDeleteUser struct {
	AccountToken string `json:"-"`
	UID          string `json:"-"`
	Password     string `json:"password"`
	JWTToken     string `json:"-"`
}

type InputGetAllUsersByID struct {
	IDs []int `json:"ids"`
}

type UserFilters struct {
	IDs    *[]int64  `json:"ids"`
	UIDs   *[]string `json:"uids"`
	Emails *[]string `json:"emails"`
}

type InputGetListUser struct {
	Page     int         `json:"page"`
	PageSize int         `json:"page_size"`
	OrderBy  string      `json:"order_by"`
	OrderDir string      `json:"order_direction" example:"asc|desc"`
	Search   string      `json:"search"`
	Filters  UserFilters `json:"filters"`
}
