package types

type JWTToken struct {
	AccessToken  string      `json:"access_token"`
	RefreshToken string      `json:"refresh_token"`
	UserData     interface{} `json:"user_data"`
}
