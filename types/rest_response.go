package types

type Pagination struct {
	Page       *int        `json:"page"`
	Items      interface{} `json:"items"`
	TotalItems *int64      `json:"total_items"`
	TotalPages *int        `json:"total_pages"`
}

type RestRespone struct {
	Message string      `json:"message"`
	Payload interface{} `json:"payload"`
}
