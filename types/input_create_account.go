package types

type InputCreateEditAccount struct {
	Name                    *string `json:"name"`
	Token                   *string `json:"token"`
	AccessTokenTime         *int64  `json:"access_token_time"`
	RefreshTokenTime        *int64  `json:"refresh_token_time"`
	Domain                  *string `json:"domain"`
	VerifyEmailEndpoint     *string `json:"verify_email_endpoint"`
	ChangePasswordEndpoint  *string `json:"change_password_endpoint"`
	DialerHost              *string `json:"dialer_host"`
	DialerPort              *int64  `json:"dialer_port"`
	DialerEmail             *string `json:"dialer_email"`
	DialerPassword          *string `json:"dialer_password"`
	DialerFromMask          *string `json:"dialer_from_mask"`
	VerifyMailTitle         *string `json:"verify_mail_title"`
	ChangePasswordMailTitle *string `json:"change_password_mail_title"`
	VerifyMailBody          *string `json:"verify_mail_body"`
	ChangePasswordMailBody  *string `json:"change_password_mail_body"`
	OldPasswordCheck        *bool   `json:"old_password_check"`
	SMSCodeTimeToLive       *int    `json:"sms_code_time_to_live"`
}
