package types

type InputChangeUserPass struct {
	NewPassword string  `json:"new_password"`
	OldPassword *string `json:"old_password"`
	Code        string  `json:"code"`
	UserUID     string  `json:"user_uid"`
}
