package types

import "time"

type RequestHistory struct {
	Success   bool      `json:"success"`
	ClientIP  string    `json:"client_ip"`
	CreatedAt time.Time `json:"created_at"`
}
