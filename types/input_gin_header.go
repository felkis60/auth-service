package types

type InputGinHeader struct {
	Authorization string `header:"Authorization"`
}

type InputAccountTokenHeader struct {
	Token string `header:"Token"`
}

type InputGetPhone struct {
	Phone string `header:"phone"`
}

type InputGetPhoneAndCode struct {
	Phone string `json:"phone"`
	Code  string `json:"code"`
}
