package types

import "time"

type BruteforceHistory struct {
	ID        int64     `json:"id" gorm:"primarykey"`
	Data      string    `json:"data"`
	ClientIP  string    `json:"client_ip" gorm:"index"`
	Success   bool      `json:"success"`
	CreatedAt time.Time `json:"created_at"`
}
