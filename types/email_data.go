package types

type EmailData struct {
	From   string
	To     string
	ToMany []string
	Title  string
	Text   string
}
