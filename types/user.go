package types

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	ID                 int64          `json:"id" gorm:"primarykey"`
	UID                string         `json:"uid"`
	ExtID              *int64         `json:"ext_id"`
	AccountsID         int64          `json:"accounts_id" gorm:"uniqueIndex:users_unique_email_acc;uniqueIndex:users_unique_phone_acc;uniqueIndex:users_unique_nickname_acc"`
	Password           string         `json:"password"`
	Name               string         `json:"name"`
	LastName           string         `json:"last_name"`
	Nickname           *string        `json:"nickname" gorm:"uniqueIndex:users_unique_nickname_acc"`
	Phone              *string        `json:"phone" gorm:"uniqueIndex:users_unique_phone_acc"`
	Email              *string        `json:"email" gorm:"uniqueIndex:users_unique_email_acc"`
	NewEmail           *string        `json:"new_email"`
	NewPhone           *string        `json:"new_phone"`
	EmailVerify        bool           `json:"email_verify"`
	PhoneVerify        bool           `json:"phone_verify"`
	SecretTokenForView string         `json:"secret_token_for_view,omitempty" gorm:"-"`
	SecretToken        string         `json:"-"`
	CodeToVerify       string         `json:"code_to_verify,omitempty" gorm:"-"`
	CreatedAt          time.Time      `json:"created_at"`
	UpdatedAt          time.Time      `json:"updated_at"`
	DeletedAt          gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}
