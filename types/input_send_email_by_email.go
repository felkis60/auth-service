package types

type InputSendEmailByEmail struct {
	Email   string `json:"email"`
	Title   string `json:"title"`
	Content string `json:"content"`
}
