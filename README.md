## Запуск 
    go run .
    go run ./api/rest/server/.
    go run ./api/grpc/server/.

## Swagger
    swag init -o ./api/rest/server/docs

## GRPC Init
    protoc --go_out=. --go_opt=paths=source_relative .\proto\auth.proto
	protoc --go-grpc_out=. --go-grpc_opt=paths=source_relative .\proto\auth.proto

## Tests
    go test ./api/rest/test/. -v  