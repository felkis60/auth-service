package log

import (
	"log"
	"os"
)

func StartLogs() {

	if os.Getenv("log_file") == "true" {
		file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			log.Fatal(err)
		}
		log.SetOutput(file)
	}
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	if os.Getenv("INFOLOG") == "true" {
		PrintInfo = true
	}
}

const (
	ERR                           = "ERROR: %v"
	ERRWrongData                  = "ERROR: Wrong data"
	ERRWrongOldPass               = "ERROR: Wrong 'old_password'"
	ERRWrongPass                  = "ERROR: Wrong 'password'"
	ERREnvLoad                    = "ERROR: Failed to load .env file"
	ERRDbConnect                  = "ERROR: Failed to connect database: %v"
	ERRAutoMigrate                = "ERROR: Failed to automigrate: %v"
	ERRListen                     = "ERROR: Failed to listen: %v"
	ERRServe                      = "ERROR: Failed to serve: %v"
	ERRTokenReq                   = "ERROR: Token is required"
	ERRvehiclesUIDReq             = "ERROR: 'vehicles_uid' is required"
	ERRUIDReq                     = "ERROR: 'uid' is required"
	ERROldPassReq                 = "ERROR: 'old_password' is required"
	ERRPhoneReq                   = "ERROR: 'phone' is required"
	ERROrderUIDReq                = "ERROR: 'order_uid' is required"
	ErrNameReq                    = "ERROR: 'name' is required"
	ErrNicknameReq                = "ERROR: 'nickname' is required"
	ERRNoSuchToken                = "ERROR: No such Token"
	ERRNoSuchUser                 = "ERROR: No such user"
	ERRNoSuchVehiclesUDI          = "ERROR: No such 'vehicles_uid'"
	ERRNoSuchOrder                = "ERROR: No such order"
	ERRNoSuchUID                  = "ERROR: No such 'uid'"
	ERRNoSuchID                   = "ERROR: No such 'id'"
	ERRTokenIsOccupied            = "ERROR: Token is occupied"
	ERRRentTariffIntersect        = "ERROR: Rent Tariff intersects with other tariffs"
	ERRUsersAlreadyExistsPhone    = "ERROR: User with such phone number already exists"
	ERRUsersAlreadyExistsEmail    = "ERROR: User with such email already exists"
	ERRUsersAlreadyExistsNickname = "ERROR: User with such nickname already exists"
	ERRBadJWTToken                = "ERROR: Bad JWT token"
	ERRJWTTokenExpired            = "ERROR: JWT token expired"
	ERRInvalidVerify              = "ERROR: Vetification code is invalid"
	ERRBruteForce                 = "ERROR: Bruteforcer detected"
	ERRBadEmail                   = "ERROR: Bad email"
	ERREmailOrPhoneReq            = "ERROR: Email or Phone are required"
	ERRPassReq                    = "ERROR: Password is required"
	ERRAccessTokenTimeReq         = "ERROR: AccessTokenTime is required"
	ERRRefreshTokenTimeReq        = "ERROR: RefreshTokenTime is required"
	ERRAllReq                     = "ERROR: All field is required"
	ERRAlreadyVerified            = "ERROR: User already verified"
	ERRNotVerified                = "ERROR: User not verified"

	ERRBusyVehicle       = "ERROR: Vehicle is busy at the date"
	ERRWrongStatus       = "ERROR: Wrong 'status'"
	INF                  = "INFO: %v"
	INFReceived          = "INFO: Received: %v"
	INFStartServer       = "INFO: %v server starts"
	INFEndServer         = "INFO: %v server ends"
	INFFuncStart         = "INFO: Function %v start"
	INFFuncEnd           = "INFO: Function %v end"
	INFVehiclesSyncedREX = "INFO: Vehicles from rex is synced"
)

var (
	PrintInfo = false
)
