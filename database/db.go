package database

import (
	"log"
	"os"
	"time"

	l "gitlab.com/felkis60/auth-service/log"
	t "gitlab.com/felkis60/auth-service/types"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var Db *gorm.DB

func InitMainPostgres() {

	var dsn string
	if os.Getenv("DBHOST") != "" {
		dsn += "host=" + os.Getenv("DBHOST")
	}
	if os.Getenv("DBUSER") != "" {
		dsn += " user=" + os.Getenv("DBUSER")
	}
	if os.Getenv("DBPASSWORD") != "" {
		dsn += " password=" + os.Getenv("DBPASSWORD")
	}
	if os.Getenv("DBNAME") != "" {
		dsn += " dbname=" + os.Getenv("DBNAME")
	}
	if os.Getenv("DBPORT") != "" {
		dsn += " port=" + os.Getenv("DBPORT")
	}
	if os.Getenv("DBSSLMODE") != "" {
		dsn += " sslmode=" + os.Getenv("DBSSLMODE")
	}
	if os.Getenv("DBTIMEZONE") != "" {
		dsn += " TimeZone=" + os.Getenv("DBTIMEZONE")
	}

	newLogger := logger.New(
		log.New(log.Writer(), "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Millisecond * 300,
			LogLevel:                  logger.Error,
			IgnoreRecordNotFoundError: true,
			Colorful:                  false,
		},
	)

	var err error
	Db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{Logger: newLogger})

	if err != nil {
		log.Fatalf(l.ERRDbConnect, err.Error())
	}
}

func Migrate() {
	err := Db.AutoMigrate(&t.Account{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&t.User{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&t.BruteforceHistory{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&t.BanList{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}
}

func PaginateManual(page_ *int, pageSize_ *int, min int, max int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {

		if *page_ <= 0 {
			*page_ = 1
		}

		switch {
		case *pageSize_ > max:
			if max > 0 {
				*pageSize_ = max
			}
		case *pageSize_ < min:
			*pageSize_ = min
		}

		offset := (*page_ - 1) * *pageSize_
		return db.Offset(offset).Limit(*pageSize_)
	}
}

func Paginate(page_ *int, pageSize_ *int) func(db *gorm.DB) *gorm.DB {
	return PaginateManual(page_, pageSize_, 5, 100)
}

func DbAccountLeftJoin(baseModel, accountToken string, db *gorm.DB) *gorm.DB {
	return db.Joins("LEFT JOIN accounts ON "+baseModel+".accounts_id = accounts.id").Where("accounts.token = ?", accountToken)
}

func DbDateTimeIntersect(checkStart, checkEnd time.Time, db *gorm.DB, startField, endField string) *gorm.DB {
	if !checkEnd.IsZero() {
		db = db.Where(startField+" <= ?", checkEnd)
	}
	if !checkStart.IsZero() {
		db = db.Where(endField+" >= ?", checkStart)
	}
	return db
}
