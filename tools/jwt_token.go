package tools

import (
	"errors"
	"fmt"
	"os"
	"time"

	logs "gitlab.com/felkis60/auth-service/log"
	"gitlab.com/felkis60/auth-service/types"

	"github.com/golang-jwt/jwt"
)

func CreateJWTToken(accountToken string, user *types.User, accessTime int, refreshTime int) (*types.JWTToken, error) {

	var err error
	var token types.JWTToken
	//Creating Access Token
	atClaims := jwt.MapClaims{}
	atClaims["user_id"] = user.ID
	atClaims["user_ext_id"] = user.ExtID
	atClaims["user_nickname"] = user.Nickname
	atClaims["user_name"] = user.Name
	if user.Phone != nil {
		atClaims["user_phone"] = user.Phone
	} else {
		atClaims["user_phone"] = ""
	}
	if user.Email != nil {
		atClaims["user_email"] = user.Email
	} else {
		atClaims["user_email"] = ""
	}
	if user.Nickname != nil {
		atClaims["user_nickname"] = user.Nickname
	} else {
		atClaims["user_nickname"] = ""
	}

	atClaims["account_token"] = accountToken
	atClaims["exp"] = time.Now().Add(time.Second * time.Duration(accessTime)).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token.AccessToken, err = at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return nil, err
	}

	//Creating Refresh Token
	rtClaims := jwt.MapClaims{}
	rtClaims["user_id"] = user.ID
	rtClaims["user_ext_id"] = user.ExtID
	rtClaims["user_name"] = user.Name
	if user.Phone != nil {
		rtClaims["user_phone"] = user.Phone
	} else {
		rtClaims["user_phone"] = ""
	}
	if user.Email != nil {
		rtClaims["user_email"] = user.Email
	} else {
		rtClaims["user_email"] = ""
	}
	if user.Nickname != nil {
		rtClaims["user_nickname"] = user.Nickname
	} else {
		rtClaims["user_nickname"] = ""
	}
	rtClaims["account_token"] = accountToken
	rtClaims["exp"] = time.Now().Add(time.Second * time.Duration(refreshTime)).Unix()
	rt := jwt.NewWithClaims(jwt.SigningMethodHS256, rtClaims)
	token.RefreshToken, err = rt.SignedString([]byte(os.Getenv("REFRESH_SECRET")))
	if err != nil {
		return nil, err
	}

	token.UserData = map[string]interface{}{"user_id": user.ID, "user_ext_id": user.ExtID, "user_nickname": user.Nickname, "user_phone": user.Phone, "user_email": user.Email, "name": user.Name, "last_name": user.LastName}

	return &token, nil
}

func TokenValidAndGetData(tokenString string, secretEnv string, accountToken string) (*jwt.MapClaims, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv(secretEnv)), nil
	})
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return nil, err
	}

	_, ok = claims["exp"].(float64)
	if !ok {
		return nil, errors.New(logs.ERRBadJWTToken)
	}

	_, ok = claims["user_id"].(float64)
	if !ok {
		return nil, errors.New(logs.ERRBadJWTToken)
	}

	_, ok = claims["user_name"].(string)
	if !ok {
		return nil, errors.New(logs.ERRBadJWTToken)
	}

	_, ok = claims["user_phone"].(string)
	if !ok {
		return nil, errors.New(logs.ERRBadJWTToken)
	}

	_, ok = claims["user_email"].(string)
	if !ok {
		return nil, errors.New(logs.ERRBadJWTToken)
	}

	temp, ok := claims["account_token"].(string)
	if !ok {
		return nil, errors.New(logs.ERRBadJWTToken)
	} else if temp != accountToken {
		return nil, errors.New(logs.ERRBadJWTToken)
	}

	return &claims, nil
}
