package tools

import (
	"log"
	"strings"

	"gitlab.com/felkis60/auth-service/types"

	"gopkg.in/gomail.v2"
)

func SendEmail(account *types.Account, data *types.EmailData) error {
	m := gomail.NewMessage()
	m.SetAddressHeader("From", account.DialerFromMask, account.Name)
	if len(data.ToMany) > 0 {
		m.SetHeader("To", data.ToMany...)
	} else {
		m.SetHeader("To", data.To)
	}
	m.SetHeader("Subject", data.Title)
	m.SetBody("text/html", data.Text)

	d := gomail.NewDialer(account.DialerHost, account.DialerPort, account.DialerEmail, account.DialerPassword)

	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}

func SendEmailVerifyEmail(account *types.Account, data *types.EmailData) error {
	if strings.Contains(account.VerifyEmailEndpoint, "?") {
		data.Text = strings.Replace(data.Text, "?", "&", 1)
	}

	m := gomail.NewMessage()
	m.SetAddressHeader("From", account.DialerFromMask, account.Name)
	m.SetHeader("To", data.To)
	m.SetHeader("Subject", account.VerifyMailTitle)

	var tempText = ""
	if strings.Contains(account.VerifyMailBody, "##LINK##") {
		tempText = strings.Replace(account.VerifyMailBody, "##LINK##", account.Domain+account.VerifyEmailEndpoint+data.Text, -1)
	} else {
		tempText = account.VerifyMailBody + "<br><br>" + account.Domain + account.VerifyEmailEndpoint + data.Text
	}
	m.SetBody("text/html", tempText)

	log.Println("SendEmailVerification account.DialerHost=" + account.DialerHost)
	log.Printf("SendEmailVerification account.DialerPort=%d", account.DialerPort)
	log.Println("SendEmailVerification account.DialerEmail=" + account.DialerEmail)
	log.Println("SendEmailVerification account.DialerPassword=" + account.DialerPassword)

	d := gomail.NewDialer(account.DialerHost, account.DialerPort, account.DialerEmail, account.DialerPassword)

	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}

func SendEmailChangePass(account *types.Account, data *types.EmailData) error {
	if strings.Contains(account.ChangePasswordEndpoint, "?") {
		data.Text = strings.Replace(data.Text, "?", "&", 1)
	}

	m := gomail.NewMessage()
	m.SetAddressHeader("From", account.DialerFromMask, account.Name)
	m.SetHeader("To", data.To)
	m.SetHeader("Subject", account.ChangePasswordMailTitle)

	var tempText = ""
	if strings.Contains(account.ChangePasswordMailBody, "##LINK##") {
		tempText = strings.Replace(account.ChangePasswordMailBody, "##LINK##", account.Domain+account.ChangePasswordEndpoint+data.Text, -1)
	} else {
		tempText = account.ChangePasswordMailBody + "<br><br>" + account.Domain + account.ChangePasswordEndpoint + data.Text
	}
	m.SetBody("text/html", tempText)

	d := gomail.NewDialer(account.DialerHost, account.DialerPort, account.DialerEmail, account.DialerPassword)

	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}
