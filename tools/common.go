package tools

func GenerateRedisKeyForSMSCode(accountToken string, uid string) string {
	return "[auth-service]" + accountToken + "_" + uid + "_sms-code"
}
