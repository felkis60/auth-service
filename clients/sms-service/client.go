package client

import (
	"log"
	"os"

	smsPB "gitlab.com/felkis60/sms-service/api/grpc/app"
	"google.golang.org/grpc"
)

var SMSClientConnection *grpc.ClientConn

var SMSClient smsPB.SMSserviceClient

func Init() {
	//var opts []grpc.DialOption
	var err error
	SMSClientConnection, err = grpc.Dial(os.Getenv("DBHOST")+":"+os.Getenv("SMS_GRPC_PORT"), grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}

	SMSClient = smsPB.NewSMSserviceClient(SMSClientConnection)

}
